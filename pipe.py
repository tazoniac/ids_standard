from os import listdir, chdir, getcwd, remove, rename, mkdir
from os.path import isfile
from shutil import copyfile
from astropy.io import fits
import matplotlib.pyplot as plt
import pyraf.iraf as iraf
import pandas as pd
import numpy as np
import INTtools

#path = '/scratch/pedroap/data/20171214/'
#path = '/scratch/pedroap/data/20181201/'
#path = '/scratch/pedroap/data/20180620/'
#path = '/scratch/pedroap/data/20180627/'
path = '/scratch/pedroap/data/20160722/'
#path = '/scratch/pedroap/data/20160902/'
#path = '/scratch/pedroap/data/20170529/'
#path = '/scratch/pedroap/data/20190704/'
path = '/scratch/pedroap/data/20190801/'

# Interactivity
Interactivity_flag = True; # When False, it does not ask for the user interaction. Overrides some options

# Cosmic Ray correction:
use_previous_cosmicrayfiles = False;
AutoCosmicClean_flag = False; # If True, the correction is automatized
ManualCosmicClean_flag = True;# If True, you can correct the spectrum by yourself

# Flat
use_spatial_flat = False; # apflatten? Always False. Do not use it
multiflat_flag = False; # Use all the flats in the run?
flat_ref_file = None; # None or a .fit/.fits flat file (new flats will be calculated anyway, but not used)

# Zero (Bias) parameters
apply_bias_correction = False; # zero image will be calculated anyway, but it won't be applied if False

# Trace:
improve_traces = True; # If False, use the trace estimated by IRAF
autoaperture_flag = False; # If True, INTtools will find the aperture

# Reference arc:
arc_ref_file = None; # Either None or a .fits/.fit file
#arc_ref_file = '/scratch/pedroap/data/20160720/r1213420.fit' # 20th July 2016
#arc_ref_file = '/scratch/pedroap/data/20160902/r1230978.fit' # 20th September 2016
#arc_ref_file = '/scratch/pedroap/data/20170527/r1308194.fit' # 27th May 2017
#arc_ref_file = '/scratch/pedroap/data/20171214/r1354186.fit' # 14th December 2017
#arc_ref_file = '/scratch/pedroap/data/20180616/r1393218.fit' # 15th-20th June 2018
#arc_ref_file = '/scratch/pedroap/data/20180622/r1393937.fit' # 22nd June 2018
#arc_ref_file = '/scratch/pedroap/data/20180623/r1394009.fit'; # 23rd June 2018
#arc_ref_file = '/scratch/pedroap/data/20181130/r1429230.fit' # 31st November 2018
#arc_ref_file = '/scratch/pedroap/data/20181202/r1429451.fit' # 1st-3rd December 2018
#arc_ref_file = '/scratch/pedroap/data/20190703/r1464244.fit' # 3rd July 2019
arc_ref_file = '/scratch/pedroap/data/20190802/r1467808.fit' # 2nd August 2019


# Trimming the image:
trim = 'no';
trimsec = '[*,270:3905]'; # if trim == 'yes'


# Background Parameters:
b_function = 'chebyshev'; # bkg function
b_order = 1+0*4;
b_sample = '-18:-8,8:18'; # Location of the bkg region relatives to the center
b_naverage = -5; # Lines around "line" stacked to find the aperture. A negative number implies median
b_niterate = 0+1;
b_low_reject = 3.0; #Background lower rejection sigma
b_high_reject = 3.0; #Background upper rejection sigma
b_grow = 0.0; #Default reject growing radius. Points within a distance given by this parameter of any rejected point are also rejected.


# Parameters Controlling the Trace:
t_nsum = 10; # Number of dispersion lines to be summed at each step along the dispersion.
t_step = 10; # Step along the dispersion axis between determination of the spectrum positions.
t_funct = 'legendre'; # Default trace fitting function
t_order = 7; # Default trace function order
t_niter = 1; # Default number of rejection iterations;
t_naverage = -3; # Default number of points to average or median (neg. number)
t_nlost = 5+0*3; # Number of consecutive steps in which the profile is lost before quitting the tracing in one direction. (We do not care much abouth this param).


# Summation and Background Subtraction:
bkg = 'fit'; # Type of bkg subtraction: 'none', 'average', 'median', 'minimum', 'fit'
weights = 'variance'; # 'variance' for optimal extraction. Alternative: 'none'
clean = 'yes'; # Remove very deviant points. If 'yes', weights='variance' is required
lsigma = 3.0
usigma = 3.0

# -------------------------------------------------

# Non-interactive mode 
improve_traces = improve_traces and Interactivity_flag;
autoaperture_flag = autoaperture_flag or not(Interactivity_flag);
ManualCosmicClean_flag = ManualCosmicClean_flag and Interactivity_flag
iraf_mode = 'h';
if Interactivity_flag:
	iraf_mode = 'al';

# Check reference arc exists:
if( arc_ref_file is not None): assert(isfile(arc_ref_file)), ' Error: '+arc_ref_file+' NOT FOUND'
if( arc_ref_file is not None): assert(isfile(arc_ref_file)), ' Error: '+arc_ref_file+' NOT FOUND'
# --------------------------------------------------------------------------------------
print("This script reduces the spectra observed by IDS (INT, La Palma)")
print("    If you want to use a reference identified spectrum, copy the .fit file (not the image, the output)")
if( arc_ref_file is not None):
	mssg = "of iraf.identify in database/id/"+arc_ref_file.split('[')[0];
	mssg = mssg.replace('//','/');
	print(mssg );
	del(mssg);

# ----------------------------------------- PREPARE THE LISTS ---------------------------
INTtools.Initialize( path, preserve_cr_files = use_previous_cosmicrayfiles );
cwd = getcwd();

if( arc_ref_file is not None):
	temp_path = cwd+'/database/id/'+arc_ref_file;
	temp_path = temp_path.replace('//','/').split('[')[0];
	if not isfile(temp_path):
		print('No identified spectrum %s'%temp_path);
		print('Please, make the file '%temp_path);
	del(temp_path)
	
list_of_files = listdir(path);
list_of_files.sort();
fits_files = []; # keep only fit or fits files

logfile = path;
for f in list_of_files:
	f = f.lower()
	if(f.endswith('.ms.fit') or f.endswith('.ms.fits') ):
		remove(path+f)
		continue;
	if(f.endswith('_out.fit') or f.endswith('_out.fits') ):
		remove(path+f)
		continue;
	if(f.endswith('.flat.fit') or f.endswith('.flat.fits') ):
		continue;
	if(f.endswith('.fit') or f.endswith('.fits') ):
		fits_files.append(f); # keep only fit or fits files
	if(f.startswith('run_log_') and f.endswith('.int') ):
		logfile = path+f;  # get the log file

del(list_of_files);

# Explore the log file to get the type of each spectra
diario = INTtools.read_log_file_INT( logfile );

# Create the lists (.lis files):
is_in_the_log = INTtools.create_list_files( fits_files, diario, path=path );

# Use all the flats of the run
if multiflat_flag:
	INTtools.flats_all( path );

# Exclude fits_files which are not in the log:
list_of_files = [fits_files[n] for n,v in enumerate(is_in_the_log) if v]
fits_files = list_of_files[:];
del(list_of_files);


# Save the list of the available files:
fhandle = open("files.lis","w");
for name in fits_files:
	fhandle.write(name+'[1]\n');
fhandle.close();
del(fhandle)


#--------------------------------------------------
#		COPY ORIGINAL OBJECT FILES
#--------------------------------------------------
# Create the list of the output files:
print('Copying...')
f_obj = open('obj.lis','r');
f_out = open('out.lis','w');
for line in f_obj:
	line = line.replace('\n',''); # Avoid the \n
	name = line.split('.')[-2];
	ext = (line.split('.')[-1])[:-3];
	new_name = name + '_out.fits'; # It must be .fits
	f_out.write(new_name + '[0]\n');
	# Delete the existing output file
	try:	remove(new_name); # Try to remove the output file (if exists)
	except:	pass;
	# Copy the object file
	iraf.imcopy(line, output=new_name);
	# Use itself as reference:
	iraf.hedit(images=new_name, add='yes', fields='REFSPEC1', value=new_name, addonly='yes', verify='no')
	# Delete the existing aperture file
	try:	remove(name+ '_out.ms.fits'); # The aperture (if exists)
	except: pass;
f_out.close();
f_obj.close();
del(f_obj, f_out);
print('Output files copied and saved in out.lis\n')


#--------------------------------------------------
#		COPY ORIGINAL ARC FILES
#--------------------------------------------------
# Create the list of the output files:
print('Copying...')
f_arc = open('arc.lis','r');
f_out = open('arcout.lis','w');
for line in f_arc:
	line = line.replace('\n',''); # Avoid the \n
	name = line.split('.')[-2];
	ext = (line.split('.')[-1])[:-3];
	new_name = name + '_arc.fits'; # It must be .fits
	f_out.write(new_name + '[0]\n');
	# Delete the existing output file
	try:	remove(new_name); # Try to remove the output file (if exists)
	except:	pass;
	# Copy the object file
	iraf.imcopy(line, output=new_name);
	# Use itself as reference:
	iraf.hedit(images=new_name, add='yes', fields='REFSPEC1', value=new_name, addonly='yes', verify='no')
	# Delete the existing aperture file
	try:	remove(name+ '_arc.ms.fits'); # The aperture (if exists)
	except: pass;
f_out.close();
f_arc.close();
del(f_arc, f_out);
print('Arc files copied and saved in arcout.lis\n')



#--------------------------------------------------
#		TRIM COPIED OBJECT FILES
#--------------------------------------------------
# Create the list of the output files:
if(trim.lower()=='yes'):
	print('Trimming...')
	f_out = open('out.lis','r');
	for line in f_out:
		line = line.replace('\n',''); # Avoid the \n
		try:	remove('temp.fits'); # Try to remove the output file (if exists)
		except:	pass;
		iraf.imcopy(line+trimsec, output='temp.fits');
		line = line.split('[')[0]; # Avoid [.]
		remove(line);
		rename('temp.fits',line);
	f_out.close();
	del(f_out);
	print('Output files trimmed in out.lis\n')


try: remove('Zero.fits')
except:	pass;
	
try: remove('Flat.fits')
except:	pass;


#--------------------------------------------------
#		INITIAL SET UP OF CCDRED
#--------------------------------------------------
# Set ccdred
iraf.noao(); # Load noao
iraf.noao.imred(); # Load imred
iraf.noao.imred.ccdred(); # Load ccdred
iraf.noao.imred.ccdred.unlearn();
iraf.noao.imred.ccdred.mode = iraf_mode;
iraf.noao.imred.ccdred.ccdproc.mode = iraf_mode;
iraf.noao.imred.ccdred.setinst('kpnoheaders')
iraf.noao.imred.ccdred.mode = iraf_mode;
plt.ion(); # Intercative mode


#--------------------------------------------------
#		JULIAN TIME COMPUTATION
#--------------------------------------------------
# Heliocentric Julian Date:
iraf.setjd.unlearn();
iraf.setjd.setParam('observatory', 'OBSERVAT');
iraf.setjd.setParam('epoch', 'CAT-EPOC');

# Get the julian date for objects and arcs at the same time:
# 1) Create a temp. file with the combination of out.lis and arcout.lis
# Common list:
f_temp = open('temp.lis','w');

# First with the objects...
f_read = open('out.lis','r'); 
for line in f_read:
	f_temp.write(line);
f_read.close();
# ...then the arcs
f_read = open('arcout.lis','r'); 
for line in f_read:
	f_temp.write(line);
f_read.close();
f_temp.close(); # Close the temp. file

# 2) Compute the Julian Dates
iraf.setjd.setParam('images', '@temp.lis');
iraf.setjd( images = '@temp.lis' );

# 3) Remove the file with both arcs and objects
remove('temp.lis');



#--------------------------------------------------
#			BIAS
#--------------------------------------------------
# Now save the statistics of the bias in BiasTableStat.txt
iraf.imstat('@bias.lis', Stdout = cwd+'/BiasTableStat.txt');
BiasTableStat = pd.read_table('BiasTableStat.txt', comment='#', header=None, names=['IMAGE','NPIX','MEAN','STDDEV','MIN','MAX'], delim_whitespace=True, skipinitialspace=True);
for n,image in enumerate(BiasTableStat['IMAGE'].values):
	image = image.split('/')[-1];
	BiasTableStat.loc[n,'IMAGE'] = image.split('.')[0]; # Get only the name of the file, not the path

# Show the statistics
BiasTableStat.plot.line(y=['MEAN'],yerr='STDDEV',title='Bias. Will be applied?: %s'%apply_bias_correction);
plt.ylabel('COUNTS');
plt.xlabel('');
plt.xticks( np.arange(len(BiasTableStat['IMAGE'].values)), rotation='vertical' );
plt.gca().set_xticklabels( BiasTableStat['IMAGE'].values );
plt.show();

if(Interactivity_flag): raw_input('\tEnter to continue...');
plt.close(); # Close the image

# Parameters of zerocombine
iraf.noao.imred.ccdred.zerocombine.unlearn();
iraf.noao.imred.ccdred.zerocombine.setParam('combine', 'median');
iraf.noao.imred.ccdred.zerocombine.setParam('reject', 'avsigclip');

# Compute the zero image
iraf.noao.imred.ccdred.zerocombine(input='@bias.lis', output='Zero.fits');
print(' Zero image: OBTAINED ')
# A Zero.fits file has been created

#--------------------------------------------------
#		CCDPROC PARAMETERS
#--------------------------------------------------
# Set ccdproc parameters (must be done before flatcombine):
iraf.noao.imred.ccdproc.mode = iraf_mode;
iraf.noao.imred.ccdproc.unlearn();
iraf.noao.imred.ccdproc.setParam('noproc','no'); # No procesing
iraf.noao.imred.ccdproc.setParam('fixpix','no');
iraf.noao.imred.ccdproc.setParam('oversca','no');
iraf.noao.imred.ccdproc.setParam('trim','no');
iraf.noao.imred.ccdproc.setParam('zerocor','no');
iraf.noao.imred.ccdproc.setParam('darkcor','no');
iraf.noao.imred.ccdproc.setParam('flatcor','no');
iraf.noao.imred.ccdproc.setParam('illumco','no');
iraf.noao.imred.ccdproc.setParam('fringecor','no');
iraf.noao.imred.ccdproc.setParam('scancor','no');
iraf.noao.imred.ccdproc.mode=iraf_mode; # When h, does not show the promt for inputs


#--------------------------------------------------
#			FLATS
#--------------------------------------------------
# Now save the statistics of the flats in FlatsTableStat.txt
iraf.imstat('@flats.lis', Stdout = cwd+'/FlatsTableStat.txt');
FlatsTableStat = pd.read_table('FlatsTableStat.txt', comment='#', header=None, names=['IMAGE','NPIX','MEAN','STDDEV','MIN','MAX'], delim_whitespace=True, skipinitialspace=True);
for n,image in enumerate(FlatsTableStat['IMAGE'].values):
	image = image.split('/')[-1];
	FlatsTableStat.loc[n,'IMAGE'] = image.split('.')[0]; # Get only the name of the file, not the path

# Show the statistics
INTtools.plotflatstats( FlatsTableStat, show = Interactivity_flag );
if(Interactivity_flag): raw_input('\tEnter to continue...');

if not(use_spatial_flat):
	# When the flat is a combination of flats	
	iraf.noao.imred.flatcombine.unlearn();
	# Now work with the flats:
	iraf.noao.imred.flatcombine.setParam('clobber','No'); # Set flatcombine params
	iraf.noao.imred.flatcombine.setParam('rdnoise','readnois'); # Set flatcombine params
	iraf.noao.imred.flatcombine.setParam('gain','gain'); # Set flatcombine params
	iraf.noao.imred.flatcombine.setParam('reject','avsigclip'); # Set flatcombine params
	iraf.noao.imred.flatcombine.setParam('combine','median'); # Set flatcombine params. # Large discrepancies in the first flat were found
	iraf.noao.imred.flatcombine.setParam('scale','exposure'); # Set flatcombine params

	# Combine the flats
	iraf.noao.imred.ccdred.flatcombine(input='@flats.lis', output='Flat.fits', mode='h', clobber='No', rdnoise='readnois'); # Call to flatcombine
	print('Flat images have been combined');

	# Add the key with the dispersion axis (2)
	iraf.hedit(images='Flat.fits', add='yes', fields='dispaxis', value=2, addonly='yes', verify='no')
	print('Flat image (1d): OBTAINED')

	# Show the flat:
	INTtools.plotflats();

	if(Interactivity_flag): raw_input('\tPress enter to continue...')
	plt.close(); # Close the image

	try: remove('Flatnorm.fits')
	except: pass;

	# Normalize the flats:
	iraf.twodspec(); # Load twodspec
	iraf.twodspec.longslit(); # Load longslit
	iraf.twodspec.longslit.response.setParam('interactive','no');
	iraf.twodspec.longslit.response.setParam('function','spline3');
	iraf.twodspec.longslit.response.setParam('order', 30);
	#if(trim.lower()=='yes'):
	#	iraf.twodspec.longslit.response.setParam('sample',trimsec);
	#else:
	#iraf.twodspec.longslit.response.setParam('sample','');
	iraf.twodspec.longslit.response(calibration='Flat.fits[0]', normalization='Flat.fits[0]', response='Flatnorm.fits', interactive='no' )
	try: remove('Flat.fits')
	except:	pass;
	print('Flats were combined. Flatnorm.fits created')
else:
	# When apflatten must be used:
	iraf.twodspec(); # Load twodspec
	iraf.twodspec.apextract(); # Load apextract
	iraf.noao.twodspec.apextract.unlearn();

	print('Setting apflatten parameters...');
	# apflatten parameters
	iraf.twodspec.apextract.apflatten.setParam('find','no'); # We provide apertures
	iraf.twodspec.apextract.apflatten.setParam('pfit','fit2d'); 
	iraf.twodspec.apextract.apflatten.setParam('readnoise','readnois');
	iraf.twodspec.apextract.apflatten.setParam('gain','gain');
	iraf.twodspec.apextract.apflatten.setParam('function','spline3');
	iraf.twodspec.apextract.apflatten.setParam('order',30);
	iraf.twodspec.apextract.apflatten.setParam('references','@out.lis');
	iraf.twodspec.apextract.apflatten.setParam('clean','yes');

	print('Setting apall parameters...');

	# Extraction Aperture:
	iraf.noao.twodspec.apextract.apall.setParam('nsum',30) # Lines around "line" stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('width',10) # Columns around the trace stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('lower',-5)
	iraf.noao.twodspec.apextract.apall.setParam('upper',5) # Limits before/after "line"
	iraf.noao.twodspec.apextract.apall.setParam('resize','yes'); # Use the range that encloses the 10% of the peak
	iraf.noao.twodspec.apextract.apall.setParam('ylevel',0.1);
	iraf.noao.twodspec.apextract.apall.setParam('extras','no') # Lines around "line" stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('format','multispec') # Lines around "line" stacked to find the aperture

	# Background: 
	iraf.noao.twodspec.apextract.apall.setParam('b_function', b_function);
	iraf.noao.twodspec.apextract.apall.setParam('b_order', b_order);
	iraf.noao.twodspec.apextract.apall.setParam('b_sample', b_sample);
	iraf.noao.twodspec.apextract.apall.setParam('b_naverage', b_naverage);
	iraf.noao.twodspec.apextract.apall.setParam('b_niterate', b_niterate);
	iraf.noao.twodspec.apextract.apall.setParam('b_low_reject', b_low_reject);
	iraf.noao.twodspec.apextract.apall.setParam('b_high_reject', b_high_reject);
	iraf.noao.twodspec.apextract.apall.setParam('b_grow', b_grow);

	# Trace control:
	iraf.noao.twodspec.apextract.apall.setParam('t_nsum', t_nsum) # Number of dispersion lines to be summed at each step along the dispersion.
	iraf.noao.twodspec.apextract.apall.setParam('t_step', t_step) # Step along the dispersion axis between determination of the spectrum positions.
	iraf.noao.twodspec.apextract.apall.setParam('t_funct', t_funct) # Default trace fitting function
	iraf.noao.twodspec.apextract.apall.setParam('t_order', t_order) # Default trace function order
	iraf.noao.twodspec.apextract.apall.setParam('t_niter', t_niter) # Default number of rejection iterations')
	iraf.noao.twodspec.apextract.apall.setParam('t_naverage', t_naverage) # Default number of points to average or median (neg. number)
	iraf.noao.twodspec.apextract.apall.setParam('t_nlost', t_nlost) # Number of consecutive steps in which the profile is lost before quitting the tracing in one direction. (We do not care much abouth this param).

	# Summation and Background Subtraction:
	iraf.noao.twodspec.apextract.apall.setParam('background', bkg);
	iraf.noao.twodspec.apextract.apall.setParam('weights', weights);
	iraf.noao.twodspec.apextract.apall.setParam('clean', clean);
	iraf.noao.twodspec.apextract.apall.setParam('lsigma', lsigma);
	iraf.noao.twodspec.apextract.apall.setParam('usigma', usigma);
	iraf.noao.twodspec.apextract.apall.setParam('gain','gain'); # CCD gain (field in the fits file)
	iraf.noao.twodspec.apextract.apall.setParam('readnoise','readnois'); # Name of the field that corresponds to the readnoise
	iraf.noao.twodspec.apextract.apall.setParam('interactive', 'no');# Interactive?

	# Call to apall:
	print('\tCalling to apall...');
	iraf.noao.twodspec.apextract.apall(input='@out.lis', nfind=1); # This does not change the input images
	# _out.fits has two images: the corrected one and the flat
	# _out.ms.fits -> 1d spectra. Only one extension

	# APERTURE CORRECTION
	# Some apertures may not be so good. Correct them:
	lst_files = [];
	fhandle = open('out.lis');
	for fitsname in fhandle:
		lst_files.append( fitsname.replace('\n','') );
	fhandle.close(); # Close the file
	del(fhandle)

	# Create the response variables	
	response = None;
	response2 = None;
	for fitsname in lst_files:
		if(improve_traces):
			response = INTtools.EditAperture(fitsname, cwd = cwd, auto = autoaperture_flag); # Returns true if changes have been applied
			response2 = autoaperture_flag;
			# If the mode is manual, it will ask unless the user has pressed enter. It the mode is auto, do not ask
			if response: response2=INTtools.AcceptButton('Call to apall again? (this will give a correction for the aperture)\n Enter means yes', show=Interactivity_flag);
			if response2: iraf.noao.twodspec.apextract.apall(input=fitsname, nfind=1); # This does not change the input images
			print('Aperture of %s corrected'%fitsname);
	del(fitsname, lst_files, response, response2);

	# Now the images .ms.fits have been created, create a list with them
	INTtools.create_file_of_images('out.lis','obj'); # This creates the list with the _out.ms files (imgobj)
	print('\tApertures of objects: OBTAINED ')

	try: remove('Flatnorm.fits')
	except:	pass;
	print('Flatnorm.fits has been removed')

	# Call to apflatten:
	print('\tCalling to apflatten');
	INTtools.ReplaceExtension( filename='out.lis', ext = None ); # Newest image
	iraf.twodspec.apextract.apflatten(input='@flats.lis', output='Flatnorm.fits', interactive='no' )
	INTtools.ReplaceExtension( filename='out.lis', ext = 0 ); # Newest image
	print('\tapflatten has finished');

# Plot the normalized flat:
INTtools.plotflat('Flatnorm.fits', trim = trim, trimsec=trimsec);
if(Interactivity_flag): raw_input('\tPress Enter')
plt.close();
# A Flatnorm.fits file has been created
print(' Flat image: OBTAINED ');

if (flat_ref_file is None):
	flat_ref_file = cwd+'/Flatnorm.fits[0]';


#--------------------------------------------------
#		BIAS AND FLATS SUBTRACTION
#--------------------------------------------------
# Customize ccdproc:
iraf.noao.imred.ccdred.ccdproc.setParam('images', '@out.lis');
iraf.noao.imred.ccdred.ccdproc.setParam('output', '@out.lis');
iraf.noao.imred.ccdred.ccdproc.setParam('ccdtype', '');
iraf.noao.imred.ccdred.ccdproc.setParam('overscan', 'no');
iraf.noao.imred.ccdred.ccdproc.setParam('interactive', 'NO');
iraf.noao.imred.ccdred.ccdproc.setParam('flatcor', 'no'); # First zero correction (if needed)
iraf.noao.imred.ccdred.ccdproc.mode = iraf_mode;

if(apply_bias_correction):
	iraf.noao.imred.ccdred.ccdproc(zerocor='Yes', zero=cwd+'/Zero.fits[0]'); #Step by step.
	INTtools.ReplaceExtension( filename='out.lis', ext = 1 ); # Newest image
	print('Bias subtraction completed!');

# Deactivate zerocor, activate flatcor
iraf.noao.imred.ccdred.ccdproc.setParam('zerocor','no');
iraf.noao.imred.ccdred.ccdproc.setParam('flatcor', 'yes');
iraf.noao.imred.ccdred.ccdproc.setParam('flat', flat_ref_file);
iraf.noao.imred.ccdred.ccdproc.setParam('mode',iraf_mode);
iraf.noao.imred.ccdred.ccdproc.mode = iraf_mode;
iraf.noao.imred.ccdred.ccdproc();

print('Flat subtraction completed!');
# "Out" files have been updated. Now they have two extensions: [0]=corrected img, [1]=the flat


# Update the output files (keep only [0] extension):
print('Updating output files (removing one extension)...')
f_out = open('out.lis','r');
for line in f_out:
	line = line.replace('\n',''); # Avoid the \n
	try:	remove('temp.fits'); # Try to remove the output file (if exists)
	except:	pass;
	iraf.imcopy(line, output='temp.fits');
	line = line.split('[')[0]; # Avoid [.]
	remove(line);
	rename('temp.fits',line);
f_out.close();
del(f_out);
print('Output files updated\n')
# Now _out.fits have one extension
#--------------------------------------------------



#--------------------------------------------------
#		APERTURES (OBJECTS)
#--------------------------------------------------
if not(use_spatial_flat):
	# This is necessary if and only if the apertures were not obtained before. 
	# Load the modules:
	iraf.noao.twodspec();
	iraf.noao.twodspec.apextract();
	iraf.noao.twodspec.apextract.unlearn();

	print('Setting apall parameters...');

	# Extraction Aperture:
	iraf.noao.twodspec.apextract.apall.setParam('nsum',30) # Lines around "line" stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('width',10) # Lines around "line" stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('lower',-5) # Limits before/after "line"
	iraf.noao.twodspec.apextract.apall.setParam('upper',5) # Limits before/after "line"
	iraf.noao.twodspec.apextract.apall.setParam('resize','yes') # Use the range that encloses the 10% of the peak
	iraf.noao.twodspec.apextract.apall.setParam('ylevel',0.1) # Use the range that encloses the 10% of the peak
	iraf.noao.twodspec.apextract.apall.setParam('extras','no') # Lines around "line" stacked to find the aperture
	iraf.noao.twodspec.apextract.apall.setParam('format','multispec') # Lines around "line" stacked to find the aperture

	# Background: 
	iraf.noao.twodspec.apextract.apall.setParam('b_function', b_function);
	iraf.noao.twodspec.apextract.apall.setParam('b_order', b_order);
	iraf.noao.twodspec.apextract.apall.setParam('b_sample', b_sample);
	iraf.noao.twodspec.apextract.apall.setParam('b_naverage', b_naverage);
	iraf.noao.twodspec.apextract.apall.setParam('b_niterate', b_niterate);
	iraf.noao.twodspec.apextract.apall.setParam('b_low_reject', b_low_reject);
	iraf.noao.twodspec.apextract.apall.setParam('b_high_reject', b_high_reject);
	iraf.noao.twodspec.apextract.apall.setParam('b_grow', b_grow);

	# Trace control:
	iraf.noao.twodspec.apextract.apall.setParam('t_nsum', t_nsum) # Number of dispersion lines to be summed at each step along the dispersion.
	iraf.noao.twodspec.apextract.apall.setParam('t_step', t_step) # Step along the dispersion axis between determination of the spectrum positions.
	iraf.noao.twodspec.apextract.apall.setParam('t_funct', t_funct) # Default trace fitting function
	iraf.noao.twodspec.apextract.apall.setParam('t_order', t_order) # Default trace function order
	iraf.noao.twodspec.apextract.apall.setParam('t_niter', t_niter) # Default number of rejection iterations')
	iraf.noao.twodspec.apextract.apall.setParam('t_naverage', t_naverage) # Default number of points to average or median (neg. number)
	iraf.noao.twodspec.apextract.apall.setParam('t_nlost', t_nlost) # Number of consecutive steps in which the profile is lost before quitting the tracing in one direction. (We do not care much abouth this param).

	# Summation and Background Subtraction:
	iraf.noao.twodspec.apextract.apall.setParam('background', bkg);
	iraf.noao.twodspec.apextract.apall.setParam('weights', weights);
	iraf.noao.twodspec.apextract.apall.setParam('clean', clean);
	iraf.noao.twodspec.apextract.apall.setParam('lsigma', lsigma);
	iraf.noao.twodspec.apextract.apall.setParam('usigma', usigma);
	iraf.noao.twodspec.apextract.apall.setParam('gain','gain'); # CCD gain (field in the fits file)
	iraf.noao.twodspec.apextract.apall.setParam('readnoise','readnois'); # Name of the field that corresponds to the readnoise
	iraf.noao.twodspec.apextract.apall.setParam('interactive', 'no');# Interactive?

	# Call to apall:
	print('\tCalling to apall...');
	iraf.noao.twodspec.apextract.apall(input='@out.lis', nfind=1); # This does not change the input images
	# _out.fits has two images: the corrected one and the flat
	# _out.ms.fits -> 1d spectra. Only one extension

	# APERTURE CORRECTION
	# Some apertures may not be so good. Correct them:
	lst_files = [];
	fhandle = open('out.lis');
	for fitsname in fhandle:
		lst_files.append( fitsname.replace('\n','') );
	fhandle.close(); # Close the file
	del(fhandle)

	# Create the response variables	
	response = None;
	response2 = None;
	for fitsname in lst_files:
		if(improve_traces):
			response = INTtools.EditAperture(fitsname, cwd = cwd, auto = autoaperture_flag); # Returns true if changes have been applied
			response2 = autoaperture_flag;
			# If the mode is manual, it will ask unless the user has pressed enter. It the mode is auto, do not ask
			if response: response2=INTtools.AcceptButton('Continue with the next star? (NO will make IRAF get another correction for this aperture)\n Enter means yes', show=Interactivity_flag);
			if not(response2): iraf.noao.twodspec.apextract.apall(input=fitsname, nfind=1); # This does not change the input images
			print('Aperture of %s corrected'%fitsname);
	del(fitsname, lst_files, response, response2);

	# Now the images .ms.fits have been created, create a list with them
	INTtools.create_file_of_images('out.lis','obj'); # This creates the list with the _out.ms files (imgobj)
	print('\tApertures of objects: OBTAINED ')
# --------------------------------------------------------------------	



# --------------------------------------------------------------------	
# Create the list of the output files:
print('Updating output files (removing one extension)...')
f_out = open('out.lis','r');
for line in f_out:
	line = line.replace('\n',''); # Avoid the \n
	try:	remove('temp.fits'); # Try to remove the output file (if exists)
	except:	pass;
	iraf.imcopy(line, output='temp.fits');
	line = line.split('[')[0]; # Avoid [.]
	remove(line);
	rename('temp.fits',line);
	# Remove the associated aperture file .ms.fits:
	#try: remove( line.split('.')[0]+'.ms.fits' );
	#except: pass;
	# Remove the associated aperture file .ms.fits:
	#line = line.replace('/','_');
	#line = line.split('.')[0];
	#try: remove( './database/ap'+line );
	#except: pass;
f_out.close();
del(f_out);
print('Output files updated\n')

# Now we have a single extension
INTtools.ReplaceExtension('out.lis',0);
INTtools.create_file_of_apertures( 'out.lis', 'apobj.lis', cwd = cwd+'/', appendix = '');
print(' Apertures (objects): FINISHED (see database folder) ')
#--------------------------------------------------


#--------------------------------------------------
#		APERTURES (ARCS)
#--------------------------------------------------
# Summation and Background Subtraction:
iraf.noao.twodspec.apextract.apall.setParam('background', 'none');
iraf.noao.twodspec.apextract.apall.setParam('interactive', 'no');# Interactive?
#iraf.noao.twodspec.apextract.apall.setParam('apertures', '@apobj.lis');# 
iraf.noao.twodspec.apextract.apall.setParam('references', '@out.lis');#
iraf.noao.twodspec.apextract.apall.setParam('input', '@arcout.lis');#

# Call to apall:
print('\tCalling to apall...');
iraf.noao.twodspec.apextract.apall(input='@arcout.lis', nfind=1); # This does not change the input images

# .fits -> 1d spectra. Only one extension
# Since no bkg subtraction has been applied, arcs have no .ms.fits associated
INTtools.create_file_of_apertures( 'arcout.lis', 'aparc.lis', cwd = cwd+'/', appendix = '');
print(' Apertures (arcs): OBTAINED (see database folder) ')
#--------------------------------------------------



#--------------------------------------------------
#		LINES IDENTIFICATION
#--------------------------------------------------
# Identify parameters
iraf.noao.identify.setParam('section','middle column'); # Disp. is along a column
iraf.noao.identify.setParam('nsum','15'); # Number of columns close to the fiducial one
iraf.noao.identify.setParam('function','legendre'); # Spline function
iraf.noao.identify.setParam('order',4); # function order
iraf.noao.identify.setParam('autowrite','yes'); # Write to database?
iraf.noao.identify.setParam('niterate', 1); # Number of rejection iterations

# Show parameters
print('\n'+15*'*'+' Identify '+15*'*');
print('section: '+iraf.noao.identify.section);
print('nsum: '+ str(iraf.noao.identify.nsum));
print('function: '+iraf.noao.identify.function);
print('order: '+str(iraf.noao.identify.order) );
print('autowrite?: '+str(iraf.noao.identify.autowrite) );
print('niterate: '+str(iraf.noao.identify.niterate) );
#--------------------------------------------------


#--------------------------------------------------
# Prepare the folders: 
INTtools.directoriesmaker( path );

if( arc_ref_file is None):
	# Find a pre-existing file:
	arcfile = INTtools.FindIdentifiedSpectrum(path, cwd);
	if(arcfile is None):
		# Manually. Use the first arc:
		f_arc = open('arcout.lis');
		arcfile = f_arc.readline();
		f_arc.close();
		arcfile = arcfile[:-1]; # Do not include \n
		arcfile = arcfile.replace('_arc.fit','.fit');
		arcfile = arcfile.replace('.fits','.fit');
		arcfile = arcfile.split('[')[0]+'[1]';
		del(f_arc)

		print(' Sorry, you will have to identify the lines manually in\n'+arcfile)
		iraf.noao.identify( images=arcfile );
		# This generates an _arc.fit file in database/id/...
		arc_ref_file = arcfile.split('[')[0];
	else:
		# Pre-existing arc calibration
		print(' The arc image:\n\t '+arcfile+'\n will be used')
		arc_ref_file = arcfile;
else:
	# Use the user's choice
	print(' The arc image:\n\t '+arc_ref_file+'\n will be used')	

# Add extension [1] in case it is needed
if(arc_ref_file[-1]!=']'): arc_ref_file += '[1]'

# Reidentify parameters
iraf.noao.reidentify.setParam('nsum','15'); # Number of columns close to the fiducial one
iraf.noao.reidentify.setParam('section','middle column'); # Disp. is along a column
iraf.noao.reidentify.setParam('verbose','yes'); # Verbose
iraf.noao.reidentify.setParam('images','@arcout.lis'); #
iraf.noao.reidentify.setParam('reference',arc_ref_file); #
iraf.noao.reidentify.setParam('database',cwd+'/database'); #
iraf.noao.reidentify.setParam('mode',iraf_mode); #

# Show parameters
print('\n'+15*'*'+' Reidentify '+15*'*');
print('section: '+iraf.noao.reidentify.section);
print('nsum: '+str(iraf.noao.reidentify.nsum) );
print('verbose: '+str(iraf.noao.reidentify.verbose));
print('images: '+iraf.noao.reidentify.images);
print('reference: '+iraf.noao.reidentify.reference);
print('database: '+str(iraf.noao.reidentify.database) );


print('Calling to reidentify...');
iraf.noao.reidentify(images='@arcout.lis',Stdout=cwd+'/reident.txt', mode = iraf_mode );

# Show that information on screen
INTtools.DisplayReidentifyOutput();
if(Interactivity_flag): raw_input('Press enter...');
#--------------------------------------------------



#--------------------------------------------------
#		ARC-OBJECT ASSIGNATION
#--------------------------------------------------
print('ARC Assignation');
#iraf.noao.onedspec();
iraf.noao.onedspec.refspectra.unlearn();
#iraf.noao.onedspec.refspectra.setParam('input','@out.lis');
#iraf.noao.onedspec.refspectra.setParam('references','@arc.lis');
#iraf.noao.onedspec.refspectra.setParam('override','yes');
#iraf.noao.onedspec.refspectra();

iraf.refspec.setParam('override','yes'); # Override prev. assignments
iraf.refspec.setParam('confirm','no'); # Confirm the assignment?
iraf.refspec.setParam('select','interp'); # Which reference should I choose?
iraf.refspec.setParam('verbose','yes');
iraf.refspec.setParam('input','@out.lis');
iraf.refspec.setParam('references','@arcout.lis');
#iraf.refspec.setParam('apertures','@apobj.lis');
iraf.refspec.setParam('ignoreaps','No');
iraf.refspec.setParam('mode',iraf_mode);
#iraf.refspec.setParam('refaps','@aparc.lis');
iraf.refspec( input='@imgobj.lis', references='@arcout.lis', apertures = '', refaps = '', mode = iraf_mode );
#--------------------------------------------------


#--------------------------------------------------
#		LINES CORRELATION
#--------------------------------------------------
# Dispcor requires a copy
f_read = open('out.lis','r'); 
f_write = open('cross.lis','w'); 
for line in f_read:
	line = line.replace('\n','');
	name = line.split('[')[0];
	new_name = name.replace('_out.fits', '_cross.fits');
	iraf.imcopy( line, output=new_name );
	f_write.write( new_name+'[0]\n' );
f_write.close();
f_read.close();

# Remove the [0] extension in cross.lis
INTtools.ReplaceExtension('cross.lis', None);

# Problems with dispcor: for any reason, the .fits files created in "database" subfolders must have no .fits ext.
INTtools.RenameArcs( path );

# Parameters of dispcor
iraf.onedspec.dispcor.setParam('verbose', 'yes');
iraf.onedspec.dispcor.setParam('log', 'no');
iraf.onedspec.dispcor.setParam('input', '@imgobj.lis');
iraf.onedspec.dispcor.setParam('output', '@cross.lis');
iraf.onedspec.dispcor.setParam('mode', iraf_mode);
iraf.onedspec.dispcor();

# Plot the results:
INTtools.ShowResults('cross.lis', show=Interactivity_flag);
if(Interactivity_flag): raw_input('Press to continue');
#--------------------------------------------------



#--------------------------------------------------
#			COSMIC RAYS
#--------------------------------------------------
# Read the out file:
fhandle = open('cross.lis');

# With AutoCosmicClean_flag
for line in fhandle:
	if not(AutoCosmicClean_flag): break;

	line = line.replace('\n','');
	# When we have no previous .cr files
	if not(use_previous_cosmicrayfiles):
		INTtools.AutoClearCosmicRays( line );
		print('\nNow you have bad pixel mask created by AutoClearCosmicRays for %s'%line)
	
	# When we have the previous .cr files
	print('AutoClearCosmicRays is running for: '+line);
	# Read the correspondent .cr file:
	name = line.split('[')[0];
	name = name.replace('.fits', '.cr');
	name = name.replace('.fit', '.cr');
	
	# Try to read the data
	try:
		mask = np.genfromtxt( name, delimiter = ' ');
	except:
		print(name+' not found. Create it:')
		INTtools.AutoClearCosmicRays( line );
		mask = np.genfromtxt( name, delimiter = ' ');
		continue;
	if(len(mask)==0):
		print(name+' has no bad pixels');
		continue;
	if(len(mask.shape)==1):
		mask = mask.reshape(1,2);
	for n in range(mask.shape[0]):
		iraf.imreplace.setParam( 'images',line+'[1] %i:%i'%(int(mask[n,0]),int(mask[n,0])) )
		iraf.imreplace.setParam( 'value', mask[n,1]);
		iraf.imreplace( images = line+'[1] %i:%i'%(int(mask[n,0]),int(mask[n,0])), value = mask[n,1]);
	del(mask)
	print(line + ' has been AUTO corrected from Cosmic Rays');
# ------------------------------

fhandle.seek(0);

# ------------------------------
# Without AutoCosmicClean_flag
for line in fhandle:
	if not(ManualCosmicClean_flag): break;

	line = line.replace('\n','');
	# When we have no previous .cr files
	if not(use_previous_cosmicrayfiles ):
		INTtools.CleanCosmicRays( line );
		print('Now you have bad pixel mask created by you!')
	
	# When we have the previous .cr files
	# Read the correspondent .cr file:
	name = line.split('[')[0];
	name = name.replace('.fits', '.cr');
	name = name.replace('.fit', '.cr');
	
	# Try to read the data
	try:
		mask = np.genfromtxt( name, delimiter = ' ');
		mask = mask.reshape(mask.shape[0],2);
	except:
		print(name+' not found. Create it:')
		INTtools.CleanCosmicRays( line );
		mask = np.genfromtxt( name, delimiter = ' ');
		continue;
	if(len(mask)==0):
		print(name+' has no bad pixels');
		continue;
	accept = INTtools.AcceptButton(show=Interactivity_flag);
	if not accept:
		remove( name );
		continue;
	if(len(mask.shape)==1):
		mask = mask.reshape(1,2);
	for n in range(mask.shape[0]):
		iraf.imreplace.setParam( 'images',line+'[1] %i:%i'%(int(mask[n,0]),int(mask[n,0])) )
		iraf.imreplace.setParam( 'value', mask[n,1]);
		iraf.imreplace( images = line+'[1] %i:%i'%(int(mask[n,0]),int(mask[n,0])), value = mask[n,1]);
	del(mask)
	print(line + ' has been corrected from Cosmic Rays');

# ------------------------------
		
fhandle.close();

plt.close();
INTtools.ShowResults('cross.lis', show = Interactivity_flag);
INTtools.SaveSpectrumAsImage('cross.lis');
plt.close('all');
