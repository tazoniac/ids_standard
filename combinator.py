from os import remove, rename, chdir, mkdir, listdir
import matplotlib.pyplot as plt
from os.path import isdir, isfile
from astropy.io import fits
import pyraf.iraf as iraf
from shutil import copyfile
from numpy import arange

# This code combines the observed spectra of the same object.
# Uses the files reduced by pipe.py

# Parameters the user must provide:
path = '/scratch/pedroap/data/'; # Parent folder to find the reduced spectra (using pipe.py):
output_folder = '/scratch/pedroap/IDSpipeline/Reduced/'; # Where do you want to save the results?
figures_folder = '/scratch/pedroap/IDSpipeline/figures2/'; # Where do you want to save the figures?


color_lst = ['b','r','g','orange','purple','m']
#------------ helpful function -------
def parseint(word):
	try:
		int(word);
		return(True);
	except:
		return(False);
# ----------------------------------

lst = listdir(path);
# Include also the current folder:
lst.append('');
lst_dir = [element for element in lst if isdir((path+element).replace('//','/'))];
lst_dir = [element for element in lst if parseint(element)]; # The name must be a date (or at least, a number)
del(lst);

spectrum_counter = 0;
AD = dict(); # AD[object_name] = list_of_files (AD=Associative dictionary)
for folder in lst_dir:
	path_tmp = path + '/'+folder+'/';
	path_tmp = path_tmp.replace('//','/');
	lst = listdir( path_tmp );
	lst = [element for element in lst if element.endswith('_cross.fits')];
	for element in lst:
		# Include the path
		complete_name = path_tmp + element;
		complete_name = complete_name.replace('//','/');

		# Open the fits file and read the object name
		fitshandle = fits.open(complete_name); # Open the file
		obj_name = fitshandle[0].header['OBJECT'].upper(); # Get the name of the object
		obj_name = obj_name.strip();
		if(obj_name.startswith('PR_')): obj_name = 'P_'+obj_name[3:]; # Different naming conventions between observers. Only for Pristine.
		fitshandle.close(); # Close the file
		AD[obj_name] = AD.get(obj_name,[]) + [complete_name]; # Add it to the dictionary
		spectrum_counter+=1;
del(fitshandle);

# Now get a set of unique object names:
obj_names_lst = list(set(AD.keys()));
print('There are %i objects in %i spectrum files.'%(len(obj_names_lst), spectrum_counter) );

# -------------------------------
# Set scombine parameters
iraf.scombine.unlearn();
iraf.scombine.setParam('combine','sum');
iraf.scombine.setParam('group','all');
iraf.scombine.setParam('reject','sigclip');
iraf.scombine.setParam('gain','gain');
iraf.scombine.setParam('scale','exposure');
iraf.scombine.setParam('mode','h');# No questions
iraf.scombine.setParam('input','@tocombine.lis');

# Check the output folder exists (otherwise create it):
if not isdir(output_folder): mkdir(output_folder);


# Now for each object, combine its spectra:
for obj_name in obj_names_lst:
	# Get the multiplicity:
	Nspectra = len(AD[obj_name]);
	output_name = output_folder+'/'+obj_name+'.fits';
	output_name = output_name.replace('//','/');
	iraf.scombine.setParam('output', output_name);

	# if there is just one spectrum, just copy the file:
	if(Nspectra==1):
		copyfile( AD[obj_name][0], output_name);
		last = 1; # In which image is the 1d spectrum saved?
	else:
		# Remove the previous file if exists:
		if isfile(output_name): remove(output_name);

		# Save the files in a tocombine.lis
		fhandle = open('tocombine.lis','w');
		for name in AD[obj_name]:
			fhandle.write(name+'[1]\n');
		fhandle.close();

		# if there are more than one spectrum, combine them
		iraf.scombine();
		last = 0; # In which image is the 1d spectrum saved?
	# Keep the last spectrum
	iraf.imcopy(output_name+'[%i]'%(last),'temp.fits');
	pass;
	remove(output_name);
	rename('temp.fits', output_name);
	

del(fhandle);
remove('tocombine.lis');

# ------------------------------------------------

# Check the folder of the figures exists (otherwise create it):
if not isdir(figures_folder): mkdir(figures_folder);


# For each combined spectra, save the images:
lst = listdir(output_folder);
print('Printing...');
for element in lst:
	if not (element.endswith('.fits') ): continue;
	obj_name = element.split('.fits')[0];
	
	element = output_folder + '/'+element;
	element = element.replace('//', '/');
	fhandle = fits.open( element ); # Open the file
	n = len(fhandle)-1; # Image of the 1D spectrum
	y = fhandle[n].data.copy();
	wavelength = arange(len(y));

	wavelength = float(fhandle[n].header['CRVAL1']) + float(fhandle[n].header['CDELT1'])*(wavelength-int( fhandle[n].header['CRPIX1']   ));
	texp = float(fhandle[n].header['EXPTIME']); # Total exposure time
	if 'NCOMBINE' in fhandle[n].header.keys():
		ncomb = int(fhandle[n].header['NCOMBINE']);
		title_txt = obj_name + ' t. exp = %.1f s'%texp + ' ncomb = %i'%ncomb
	else:
		title_txt = obj_name + ' t. exp = %.1f s'%texp + ' ncomb = 1'
		ncomb = 1;
	fhandle.close(); # Close the file

	color = color_lst[min([ncomb-1, len(color_lst)-1])];

	# Representation
	fig = plt.figure(facecolor='w', figsize=(19.2, 9.46));
	plt.plot(wavelength, y, color);
	plt.plot(wavelength, 0*wavelength,'k');
	plt.title(title_txt);

	# complete name	
	complete_name = figures_folder + '/fig_'+obj_name;
	complete_name = complete_name.replace('//','/');
	complete_name = complete_name.replace('.','');
	fig.savefig( complete_name+'.png');
	plt.close('all'); # Sometimes plt.close() fails

	print(element + ' finished');

print('There were %i objects in %i spectrum files.'%(len(obj_names_lst), spectrum_counter) );
