from os import remove, rename, chdir, mkdir, listdir
import matplotlib.pyplot as plt
from os.path import isdir, isfile
from astropy.io import fits
from shutil import copyfile
from numpy import arange, float32
from INTtools import GetDopplerVelocity

# This code combines the observed spectra of the same object.
# Uses the files reduced by pipe.py

# Parameters the user must provide:
path = '/scratch/pedroap/data/'; # Parent folder to find the reduced spectra (using pipe.py):
output_filename = 'Doppler.txt'; # Where do you want to save the results?


color_lst = ['b','r','g','orange','purple','m']
#------------ helpful function -------
def parseint(word):
	try:
		int(word);
		return(True);
	except:
		return(False);
# ----------------------------------

lst = listdir(path);
# Include also the current folder:
lst.append('');
lst_dir = [element for element in lst if isdir((path+element).replace('//','/'))];
lst_dir = [element for element in lst if parseint(element)]; # The name must be a date (or at least, a number)
del(lst);

spectrum_counter = 0;
AD = dict(); # AD[object_name] = list_of_files (AD=Associative dictionary)
for folder in lst_dir:
	path_tmp = path + '/'+folder+'/';
	path_tmp = path_tmp.replace('//','/');
	lst = listdir( path_tmp );
	lst = [element for element in lst if element.endswith('_cross.fits')];
	for element in lst:
		# Include the path
		complete_name = path_tmp + element;
		complete_name = complete_name.replace('//','/');

		# Open the fits file and read the object name
		fitshandle = fits.open(complete_name); # Open the file
		obj_name = fitshandle[0].header['OBJECT'].upper(); # Get the name of the object
		if(obj_name.startswith('PR_')): obj_name = 'P_'+obj_name[3:]; # Different naming conventions between observers. Only for Pristine.
		fitshandle.close(); # Close the file
		AD[obj_name] = AD.get(obj_name,[]) + [complete_name]; # Add it to the dictionary
		spectrum_counter+=1;
del(fitshandle);

# Now get a set of unique object names:
print('There are %i objects in %i spectrum files.'%(len(AD.keys()), spectrum_counter) );


# Open the output file
#fhandle = open(output_filename,'w');

VD = dict(); # Velocity dictionary

# Now evaluate each object individually
for obj_name, spectrum_lst in AD.iteritems():
	for filename in spectrum_lst:
		v = GetDopplerVelocity( filename );
		VD[obj_name] = VD.get(obj_name,[]) + [v]; # Add it to the dictionary
	VD[obj_name] = float32(VD.get(obj_name,[]));
	del(spectrum_lst);


	
print('Velocitites obtained for all the stars');
#fhandle.close();
