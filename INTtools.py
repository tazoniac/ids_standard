from numpy import array, percentile, log10, arange, median, sign, fabs
from numpy import logical_not, int32, float32, linspace, sqrt, exp, infty
from os import remove, rename, chdir, getcwd, mkdir, listdir
import matplotlib.pyplot as plt
from astropy.io import fits
from numpy.random import shuffle
from os.path import isfile, isdir
from scipy.signal import medfilt
from scipy.interpolate import interp1d
from scipy.optimize import least_squares
from numpy.linalg import solve
import re

Common_lines_names = ['NeV','Hmu','OII','Hlambda','Hk','H','Htheta','Heta','HeI','CaII K','CaII H','He','SII','Hd','G','Hg','OIII','Hb','OIII','OIII','OIII','Mg'];
Common_lines_wavelengths = array([3426.85, 3721,3728.3, 3734, 3750, 3770., 3797,3835,3889, 3934.777, 3969.588, 3971.19, 4072.3, 4102.89, 4305.61, 4341.68, 4364.436, 4862.68, 4932.603, 4960.295, 5008.24, 5176.7]);

Common_lines_names2 = ['HeI', 'NIV', 'OII', 'OII', 'OII', 'SiIV', 'SiIV', 'HeI', 'SiII', 'SiII', 'OII', 'HeI', 'CIII', 'CIII', 'OII', 'CIII', 'OII', 'HeII', 'SiIV', 'NII', 'OII', 'OII', 'OII', 'HeI', 'OII', 'OII', 'OII', 'HeI', 'MgII', 'NIV', 'NIII', 'NIII', 'NIII', 'NIII', 'SiIII', 'HeII', 'SiIII', 'SiIII', 'SiIII', 'SiIII', 'NV', 'NIV', 'OII', 'SiIV', 'CIII', 'OII', 'OII', 'HeII', 'HeI', 'HeI', 'HeI', 'HeI', 'NIV', 'HeII', 'NIV', 'SiIII', 'SiIII'];
Common_lines_wavelengths2 = array([4026.33, 4057.759, 4069.636, 4072.164, 4075.868, 4088.863, 4116.104, 4120.81, 4128.054, 4130.894, 4132, 4143.76, 4153.685, 4155.8, 4185, 4186.9, 4189, 4199.87, 4212, 4237.07, 4317.139, 4319.631, 4366, 4387.928, 4414.9, 4416.97, 4452, 4471.477, 4481.13, 4513.3, 4516.12, 4519.41, 4530.84, 4534.57, 4539, 4541.59, 4552.654, 4554, 4567.872, 4574.777, 4603.73, 4607.3, 4638, 4654, 4659.06, 4661, 4676, 4685.68, 4713.37, 4921.93, 5015.6779, 5047.738, 5204.28, 5411.524, 5424.31, 5451.46, 5473.05]);

_FERRE_keywords = ['n_of_dim','n_p','npix','llimits', 'steps','multi','synthfile_internal','id','date','npca','label','transposed','comments1'];
_FERRE_keywords = _FERRE_keywords + ['comments2','comments3','comments4', 'comments5', 'comments6', 'wave', 'logw', 'vacuum', 'resolution', 'original_sampling', 'synspec', 'modo', 'invalid_code', 'constant', 'continuum', 'precontinuum', 'file_data19', 'file_data20'];
# --------------------------------
# flats_all( path );
# read_log_file_INT( filename );
# create_list_files( fits_files, diario, path='');
# plotflatstats(FlatsTableStat);
# plotflats(flf = 'flats.lis', fiducialflat = 'Flat.fits', label= 'Flats', **kwargs);
# create_file_of_images( objf = 'obj.lis', type_of_spec = 'obj' );
# create_file_of_apertures( input_filename = 'obj.lis', output_filename = 'apobj.lis', cwd='.', appendix = '_extension1_1_' );
# plotflat( filename= 'Flatnorm.fits', trim = 'no', trimsec = [], arc_list = 'arc.lis', obj_list = 'obj.lis' );
# ReplaceExtension( filename='out.lis', ext = 0 );
# directoriesmaker( path , cwd = './');
# FindIdentifiedSpectrum( path, cwd='./' );
# DisplayReidentifyOutput( filename='./reident.txt' );
# RenameArcs(original_path, cwd = None);
# Initialize(path, cwd = None, preserve_cr_files = False);
# ShowResults( namelist = 'cross.lis');
# CleanCosmicRays(namelist = 'cross.lis');
# SaveSpectrumAsImage( namelist = 'cross.lis')
# AcceptButton(mssg='Accept changes?');
# ReadAperture(fitsname, cwd = './', lowhigh=False);
# EditAperture(fitsname, cwd = './', auto = True);
# WriteAperture(fitsname, center, min_line, max_line, coeffs, cwd = './', bkg_coords=None);
# _IRAFfittingfunct( x, type_of_fit, order, min_line, max_line, coeffs);
# _MyIRAFLegendrefitting( x, y);
# _MyIRAFLinearInterpfitting( x, y);
# _StackColums(data2D, x, y, #_width = 50, auto = False);
# AutoClearCosmicRays(fitsname='cross.lis', cwd = './');
# _find_best_fit(data1D_window, trace_median, ini);
# _gaussianfit(x, y);
# _AutoTraceFinder( data2D );

# --------------------------------------------


# --------------------------------------------
def flats_all( path ):
	# Modifies flats.lis to use all the flats of the run
	# Assumes all the folders are "siblings" of "path"
	if(path[-1]=='/'):
		path = path[-2::-1];
	else:
		path = path[-1::-1];
	i = path.index('/');
	path = path[i:];
	path = path[-1::-1];
	
	lst = listdir(path);
	lst_dir = [element for element in lst if isdir(element)];

	list_of_flats = [];
	for directory in lst_dir:
		# Get the name of the directory
		directory = path + '/' + directory;
		directory = directory.replace('//','/');

		# Seek the log file:
		logfile = '';
		lst_files = listdir( directory );
		for element in lst_files:
			if( element.startswith('run_log_') and element.endswith('.int') ):
				logfile = directory+'/'+element;  # get the log file
				logfile = logfile.replace('//','/');
				logfile = logfile.replace('//','/');
				break;

		diario = read_log_file_INT( logfile );
		for element in diario['Flat']:
			element = '/'+directory+'/r'+element+'.fit[1]';
			element = element.replace('//','/');
			element = element.replace('//','/');
			list_of_flats.append(element);

	# Edit flats.lis
	f_flats = open('flats.lis','w');

	for element in list_of_flats:
		f_flats.write(element+'\n');
	f_flats.close(); # Close the file

	return(None)
# --------------------------------------------


# --------------------------------------------
def read_log_file_INT( filename ):
	# Understands the log file of IDS, and returns a dictionary with the types of files (flats, objects, bias...)
	# If no log file is provided (or filename is not a .int file), read the header of all the fit files to know their purpose.
	password = 'run_log_' in filename;
	password = password and filename.endswith('.int');
	password = password and (isfile(filename));

	# When password is False:
	# ----------------------------------
	if not(password):
		print('No log file: '+filename+' found!');
		print(' but INTtools will read the headers of all the fit files');
		filename = filename + '/';
		filename = filename.replace('//','/');
		lst_files = listdir( filename ); # Remember that now filename is a path
		lst_files = [(filename+'/'+selfile).replace('//','/') for selfile in lst_files if (selfile.lower().endswith('.fit') and '_' not in selfile)]; # Exclude non fit files and fits files from previous runs of pipe.py

		# Explore the .fit files
		filetype = [];
		filenumber = [];

		# Get the type and the r-number of each file
		for selfile in lst_files:
			fhandle = fits.open(selfile);
			lowername = fhandle[0].header['OBSTYPE'].lower();
			lowerobj = fhandle[0].header['OBJECT'].lower();
			fhandle.close();

			selfile = selfile.split('/')[-1];
			selfile = selfile.split('.')[0];
			selfile = selfile.replace('r','');

			# The particular case of focusing
			if lowerobj.startswith('focus'):
				lowername = 'focus';

			# The particular case of testing rotation
			if (lowerobj == 'rotation_test' or lowerobj.startswith('rotation') ):
				lowername = 'rotation_test';

			# The particular case of filter
			if lowerobj.startswith('hart'):
				lowername =  lowerobj;

			filenumber.append(selfile);
			filetype.append(lowername);

	else:

		# When password is True:
		# -----------------------------------

		# Open the file
		f = open(filename,'r');
		skip = False;
		while True:
			line = f.readline();
			if not(line):
				break;
			line = line.strip().lower();
			if(line.startswith('run     object              ra') ):
				skip = True;
				break;
		# Read two more lines (head of the table):
		f.readline();
		f.readline();

		# It fails when the header of the table was not found
		assert(skip),'Warning: No files in '+filename;

		# Now explore the table
		filetype = [];
		filenumber = [];
		while(True):
			line = f.readline();
			if not(line):
				break;
			line = line.strip();
			filenumber.append(line.split()[0]); # Get the number of the spectrum
			line = line[len(filenumber[-1]):].lstrip(); # Exclude the number
			# Get the type:
			line = re.findall('([^:]+):.*',line)[0];
			line = re.findall('(.*) [0-9]',line)[0].strip(); # line is the type
			filetype.append( line );
		# Close the log file
		f.close();



	# Now this is common for both options of password
	# --------------------------------------------------
	# Identify each type:
	arc_lst= list()
	hart_lst = list();
	bias_lst = list();
	sky_lst = list();
	flat_lst = list();
	object_lst= list();

	for n, name in enumerate(filetype):
		lowername = name.lower();
		# focus
		if(lowername.startswith('focus') or lowername.startswith('rotation') ):
			continue;
		# arcs
		if(lowername == 'arc' or lowername=='arc cune+cuar' or lowername=='arc cuar+cune'):
			arc_lst.append(filenumber[n]);
			continue;
		# hart
		if(lowername == 'hart l' or lowername=='hart r'):
			hart_lst.append(filenumber[n]);
			continue;
		#bias
		if(lowername == 'bias'):
			bias_lst.append(filenumber[n]);
			continue;
		#sky
		if(lowername == 'sky background'):
			sky_lst.append(filenumber[n]);
			continue;
		#flat
		if(lowername == 'flat field' or lowername == 'flat'):
			flat_lst.append(filenumber[n]);
		else:
			object_lst.append(filenumber[n]);


	diario = dict(); # log is a numpy keyword
	diario['Arc'] = arc_lst;
	diario['Bias'] = bias_lst;
	diario['Hart'] = hart_lst;
	diario['Sky'] = sky_lst;
	diario['Flat'] = flat_lst;
	diario['Obj'] = object_lst;

	return(diario);
#--------------------------------------------------



#--------------------------------------------------
def create_list_files( fits_files, diario, path=''):

	# Open the files
	f_arc = open('arc.lis','w');
	f_bias = open('bias.lis','w');
	f_flats = open('flats.lis','w');
	f_sky = open('sky.lis','w');
	f_obj = open('obj.lis','w');

	is_in_the_diario = [];

	for name in fits_files:
		if not(isfile(path+name)):
			continue;

		oldname = name[:];
		name = name[1:].split('.')[0];
		# Arc
		if(name in diario['Arc']):
			f_arc.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Bias
		if(name in diario['Bias']):
			f_bias.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Flats
		if(name in diario['Flat']):
			f_flats.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Sky
		if(name in diario['Sky']):
			f_sky.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Obj
		if(name in diario['Obj']):
			f_obj.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;

		name = name +'s'; # Try with .fits
		# Arc
		if(name in diario['Arc']):
			f_arc.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Bias
		if(name in diario['Bias']):
			f_bias.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Flats
		if(name in diario['Flat']):
			f_flats.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Sky
		if(name in diario['Sky']):
			f_sky.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Obj
		if(name in diario['Obj']):
			f_obj.write(path+oldname+'[1]\n');
			is_in_the_diario.append(True);
			continue;
		# Not found
		is_in_the_diario.append(False);


	# Close the files:
	f_arc.close();
	f_bias.close();
	f_flats.close();
	f_sky.close();
	f_obj.close();

	return(is_in_the_diario);
#--------------------------------------------------



# -------------------------------------------------
def plotflatstats(FlatsTableStat, show = True):
	# FlatsTableStat is a pandas dataframe

	# Show the statistics
	FlatsTableStat.plot.line(y=['MEAN'],title='Flats. Click to set ranges');
	plt.ylabel('COUNTS');
	plt.xlabel('');
	Nticks = len(FlatsTableStat['IMAGE'].values);
	plt.xticks( array(range(Nticks)), rotation='vertical' );
	plt.gca().set_xticklabels( FlatsTableStat['IMAGE'].values );
	plt.show();

	# If show = False, it does nothing
	if not(show): return;

	# Second part: exclude bad flats
	coord = plt.ginput(-1, timeout=120, show_clicks=True);
	plt.close(); # Close the image
	if(len(coord)==0):
		return;
	coord = array(coord)[:,0]; # Only x-coordinates
	coord.sort()
	lst = list(); # List of selected files
	
	for n in range(0, Nticks):
		if((sum(coord<n)%2)==1):
			# If it is odd (even), it is included (excluded)
			lst.append(n);
	# Read flats.lis to extract the desired files:
	counter = 0;
	filelist=[];
	fhandle = open('flats.lis');
	for line in fhandle:
		if(len(lst)==0):
			break;
		if counter==lst[0]:
			filelist.append(line);
			lst.pop(0);
		counter+=1
	fhandle.close();

	# Overwrittes flats.lis
	fhandle = open('flats.lis','w');
	for line in filelist:
		fhandle.write(line);
	fhandle.close();
	return;
#--------------------------------------------------



#--------------------------------------------------
def plotflats(flf = 'flats.lis', fiducialflat = 'Flat.fits', label= 'Flats', **kwargs):
	# Plots the first, the last and the flat of the middle
	# flf: flat list file

	# Open the file	
	fhandle = open( flf,'r');

	flat_lst = []; # Create an empty list
	for line in fhandle:
		name = line[:-4];# Do not include the [1]
		flat_lst.append(name);
	fhandle.close(); # Close the file

	# Get the number of flats
	Nflats = len(flat_lst);
	
	# Select at most 9 flats:
	y = range(0, Nflats);
	Nflats = min([Nflats,9]); # At most 9 flats
	shuffle(y);
	y = y[0:Nflats];
	y.sort();
	y[0] = 0; # Include the first one ALWAYS
	y[-1] = len(flat_lst)-1; # And the last one

	# Read the canonical flat:
	fhandle = fits.open( fiducialflat );
	data = fhandle[0].data.copy();
	fhandle.close(); # Close the file

	# Now create the plots:
	fig = plt.figure(label, figsize=(19.2, 9.46), facecolor='w' );
	counter=1;
	for n in y:
		flatfile = flat_lst[n];
		name = flatfile.split('/')[-1];
		# read this flat	
		fhandle = fits.open(flatfile);
		data1 = fhandle[1].data.copy();
		fhandle.close(); # Close the file
		# Divide by the canonical flat
		data1 = data1*1.0/data;
		# Put it in the plot:
		plt.subplot(1, Nflats, counter);
		plt.imshow( data1, vmin=0.9, vmax=1.1, cmap='seismic', **kwargs);
		#plt.imshow( data1, cmap='seismic', **kwargs);
		plt.title(name);
		plt.gca().invert_yaxis();
		counter +=1;
	plt.colorbar(extend='both')
	plt.show()

	return(fig);		
# ------------------------------------------------------



# -----------------------------------------------------
def create_file_of_images( objf = 'obj.lis', type_of_spec = 'obj' ):
	# objf: name of the file with the list of ORIGINAL objects (typically obj.lis)
	# type_of_spec = 'obj' or 'arc'
	if (type_of_spec == 'obj'):
		name = 'imgobj.lis';
	elif(type_of_spec == 'arc'):
		name = 'imgarc.lis';
	else:
		assert(False), 'Incorrect type_of_expec'

	f = open( objf, 'r');
	fw = open(name,'w');
	for line in f:
		ext = line.split(']')[-1];
		line = line.split('[')[0];
		line = line + ext;
		if(type_of_spec == 'obj'):
			line = line.replace('.','_out.ms.');
			line = line.replace('_out_out','_out');
		else:
			line = line.replace('.','.ms.');
		line = line.replace('\n','s\n');
		line = line.replace('.fitss', '.fits');
		fw.write(line);
		# for /scratch/pedroap/data/20180617/r1393353.fit[1]
		# we have: /scratch/pedroap/data/20180617/r1393353_out.ms.fits
	
	f.close();
	fw.close();
# ------------------------------------------------------



# -----------------------------------------------------
def create_file_of_apertures( input_filename = 'obj.lis', output_filename = 'apobj.lis', cwd='.', appendix = '_extension1_1_' ):
	# Creates a .lis files with the output of apall
	if(cwd[-1]=='/'):
		cwd = cwd[:-1];

	fr = open( input_filename,'r');
	fw = open( output_filename,'w');

	for line in fr:
		#line = line.replace('\n','');
		#ext = re.findall('(\[[0-9]\])',line);
		#line = line[:-len(ext)];
		#fmt = line.split('.')[-1];
		#line = line[:-len(fmt)-1]; # Exlude the last dot
		#line = cwd+'/database/ap'+line + appendix+'\n';
		#line = cwd+'/database/id/'+line
		#line = line.replace('//','/');
		#line = line.split('[')[0];
		#fw.write(line+'.fit\n');
		# for /scratch/pedroap/data/20180617/r1393353.fit[1]
		# we have: /scratch/pedroap/data/20180617/r1393353_out.ms.fits
		line = line.replace('\n','');
		line = line.replace('/','_');
		ext = re.findall('(\[[0-9]\])',line);
		line = line[:-len(ext)];
		fmt = line.split('.')[-1];
		line = line[:-len(fmt)-1]; # Exlude the last dot
		line = cwd+'/database/ap'+line + appendix+'\n';
		fw.write(line);
	
	fr.close();
	fw.close();
# ------------------------------------------------------



# -----------------------------------------------------
def plotflat( filename= 'Flatnorm.fits', trim = 'no', trimsec = [], arc_list = 'arc.lis', obj_list = 'obj.lis' ):
	# 
	fhandle = fits.open(filename);
	fig = plt.figure('Flat norm: '+filename,  figsize=(19.2, 9.46), facecolor='w');
	data = fhandle[0].data;
	fhandle.close();

	# Reference (arc):
	fhandle = open(arc_list);
	fileobj = fhandle.readline();
	fhandle.close();
	fileobj = fileobj.replace('\n','');
	fileobj = fileobj.split('[')[0];
	fhandle = fits.open( fileobj );
	refarc = fhandle[1].data;
	refarc[refarc<=0] = refarc[refarc>0].min()
	fhandle.close();

	# Reference (object):
	fhandle = open(obj_list);
	fileobj = fhandle.readline();
	fhandle.close();
	fileobj = fileobj.replace('\n','');
	fileobj = fileobj.split('[')[0];
	fhandle = fits.open( fileobj );
	refspectrum = fhandle[1].data;
	refspectrum[refspectrum<=0] = refspectrum[refspectrum>0].min()
	fhandle.close();
	# Rescale it
	Ncols = data.shape[1];
	refspectrum = refspectrum/data;
	refspectrum = refspectrum[:,int(Ncols/2):int(2*Ncols/3)].sum(1); # Keep the central part
	refspectrum = Ncols*(refspectrum-refspectrum.min())/(refspectrum.max()-refspectrum.min());

	if(trim == 'yes'):
		# Parse the trimsec string
		trimsec = trimsec.replace('[',' ');
		trimsec = trimsec.replace(']',' ');
		trimsec = trimsec.replace(',',' ');
		trimsec = trimsec.replace(':',' ');
		trimsec = trimsec.strip().split(' ');
		if(len(trimsec)<3):
			trimsec = [0, data.shape[1], 0, data.shape[0] ];
		else:
			if(trimsec[0]=='*'):
				trimsec = [0, data.shape[1], int(trimsec[1]), int(trimsec[2])];
			if(trimsec[2]=='*'):
				trimsec = [int(trimsec[0])-1, int(trimsec[1]), 0, data.shape[0]-1 ];


	# Get the percentiles
	p15 = percentile(data, 15);
	p85 = percentile(data, 85);

	p25 = percentile(data, 25);
	p75 = percentile(data, 75);

	# Create four axes
	ax0,ax1,ax2,ax3,ax4 = fig.subplots(1,5, sharey=True, sharex=True);

	# The original
	h1=ax1.imshow( data, cmap='jet');
	ax1.set_title('Original');
	fig.colorbar( h1, ax=ax1);

	# The second one
	h2 = ax2.imshow( data, cmap='binary');
	ax2.set_title('Original');
	fig.colorbar( h2, ax=ax2);

	# The third one
	h3 = ax3.imshow( data, vmin = p15, vmax = p85, cmap='jet' );
	ax3.set_title('15-85%');
	fig.colorbar( h3, ax=ax3);
	
	# The fourth one
	l_limit = min([2-p75, p25])
	u_limit = max([2-p25, p75])
	h4 = ax4.imshow( data, vmin = l_limit, vmax = u_limit, cmap='seismic' );
	ax4.set_title('25-75%');
	fig.colorbar( h4, ax=ax4);

	# Trim lines	
	ax1.vlines(trimsec[0], trimsec[2], trimsec[3],'k','--');
	ax1.vlines(trimsec[1], trimsec[2], trimsec[3],'k','--');
	ax1.hlines(trimsec[2], trimsec[0], trimsec[1],'k','--');
	ax1.hlines(trimsec[3], trimsec[0], trimsec[1],'k','--');

	ax2.vlines(trimsec[0], trimsec[2], trimsec[3],'r','--');
	ax2.vlines(trimsec[1], trimsec[2], trimsec[3],'r','--');
	ax2.hlines(trimsec[2], trimsec[0], trimsec[1],'r','--');
	ax2.hlines(trimsec[3], trimsec[0], trimsec[1],'r','--');

	ax3.vlines(trimsec[0], trimsec[2], trimsec[3],'w','--');
	ax3.vlines(trimsec[1], trimsec[2], trimsec[3],'w','--');
	ax3.hlines(trimsec[2], trimsec[0], trimsec[1],'w','--');
	ax3.hlines(trimsec[3], trimsec[0], trimsec[1],'w','--');

	ax4.vlines(trimsec[0], trimsec[2], trimsec[3],'y','--');
	ax4.vlines(trimsec[1], trimsec[2], trimsec[3],'y','--');
	ax4.hlines(trimsec[2], trimsec[0], trimsec[1],'y','--');
	ax4.hlines(trimsec[3], trimsec[0], trimsec[1],'y','--');

	# The Reference
	h0=ax0.imshow( refarc/data, cmap='jet', vmin = (refarc/data).min(), vmax = percentile(refarc/data, 95) );
	ax0.plot(refspectrum, arange(len(refspectrum)),'k' );
	ax0.set_title('Reference');
	ax0.set_yticks([0,len(refarc)]);
	ax0.set_yticklabels(['Red', 'Blue']);

	# Show
	plt.show();
# --------------------------------------------



# --------------------------------------------
def ReplaceExtension( filename='out.lis', ext = 0 ):
	# INTtools.ReplaceExtension( filename='out.lis', ext = 0 )
	# Substitutes [...] by [ext] in the files included in the list
	# if ext = None, removes the extension
	# if ext = +, increases the current extension
	foriginal = open(filename, 'r');
	fnew = open('new_'+filename,'w')

	if(ext=='+'):
		for line in foriginal:
			line = line.replace('\n',''); # Forget the '\n'
			if line.endswith(']'):
				ext = line.split('[')[-1];
				ext = int(ext.replace(']',''));
				line = line[:-2]+str(1+ext)+']\n';# Remember the \n
			else:
				# No extension
				line = line+'['+str(0)+']\n';
			fnew.write( line );		

	elif(type(ext)!=type(None)):
		for line in foriginal:
			line = line.replace('\n',''); # Forget the '\n'
			if line.endswith(']'):
				line = line[:-2]+str(ext)+']\n';# Remember the \n
			else:
				# No extension
				line = line+'['+str(ext)+']\n';
			fnew.write( line );
	else:
		# Remove the extension
		for line in foriginal:
			line = line.replace('\n',''); # Forget the '\n'
			if line.endswith(']'):
				line = line.split('[')[0]+'\n';# Remember the \n
			else:
				# No extension
				line = line+'\n';
			fnew.write( line );

	# Close the files
	fnew.close()
	foriginal.close();
	# Delete the aux file
	remove( filename );
	rename('new_'+filename, filename );
# --------------------------------------------



# --------------------------------------------
def directoriesmaker( path , cwd = './'):
	# Creates a subdirectory which path is:
	# (cwd)/database/id/path, because it is needed by iraf.identify()
	if(cwd[-1]!='/'): cwd = cwd + '/';
	if(path[0]=='/'): path = path[1:];
	if(path[-1]=='/'): path = path[:-1];# Exclude the last '/'
	folders = path.split('/');

	cwd0 = getcwd(); # Save the current working directory

	# Database folder
	chdir(cwd);
	if not isdir('database'):
		mkdir('database');
		print(cwd+'database/ created!');
	cwd = cwd + 'database/';
	chdir(cwd); # Advance

	# id folder
	if not isdir('id'):
		mkdir('id');
		print(cwd+'id/ created!');
	cwd = './id/';
	chdir(cwd); # Advance

	for folder in folders:
		if not isdir(folder):
			mkdir(folder);
			print(cwd+folder+'/ created!');
		cwd = cwd + folder + '/';
		chdir('./'+folder); # Advance

	chdir(cwd0);

	return(None)
# --------------------------------------------



# --------------------------------------------
def FindIdentifiedSpectrum( path, cwd='./' ):
	# If there is a .fit file, returns the first one. Otherwise, returns None
	# This program assumes you have used directoriesmaker
	if(cwd[-1]!='/'): cwd = cwd + '/';
	if(path[0]!='/'): path = '/'+path;
	if(path[-1]!='/'): path = path+'/';# Exclude the last '/'
	
	# Database folder
	files = listdir(cwd+'database/id'+path);
	fit_files = [n for n in files if (n[-4:]=='.fit')]
	del(files);
	if len(fit_files)>=1:
		return path+fit_files[0];
	else:
		return None;
# -------------------------------------------



# --------------------------------------------
def DisplayReidentifyOutput( filename='./reident.txt' ):
	# reident.txt is the stdout we have selected for iraf.noao.reidentify()
	# This will show the final dispersion relation for each image (spectrum)
	if not isfile(filename): 
		print('\tWarning: No '+filename+' found!')
		return None;

	# Read the file
	fhandle = open(filename, 'r');
	line = fhandle.readline();
	try:
		# Skip two lines more
		fhandle.readline();
		fhandle.readline();
		header = fhandle.readline(); # Header will contain the fourth line (the header of the table)
		# Edit the header:
		header = header.replace('  ',',');
		Nold = len(header);
		Nnew = Nold +1;
		while(Nold!=Nnew):
			Nold = Nnew;
			header = header.replace(',,',',');
			Nnew = len(header);
		header = header.replace(', ',',')+'\n';
		header = header.replace(' ','');
		header = header.replace(',',' ');
		header = header.replace('\n','');
		

	except:
		print('No information obtained from '+filename);
		return(None)

	# Show the last line of each image
	# ---------------------------------------
	print(header+'\n'+60*'-'); # Display the information
	line_old = header[:]; # keeps the previous line
	RMSmax = -9999;
	maxRMSfile = '';
	for line in fhandle:
		line = line.replace('\n','');
		line = line.strip();
		if line.startswith('Reference image'): # This means that the prev. line is the last iteration
			elements = line_old.split();
			name = elements[0];
			line_old = line_old[len(name):];
			name = name.split('/')[-1];
			name = name.split(']')[0]+']';
			line_old = '...'+name+' '+line_old;
			try:
				RMS = float(elements[-1]);
			except: continue;
			if(RMS>RMSmax): maxRMSfile = name;
			RMSmax = max([RMSmax, RMS]);
			
			print( line_old );
		line_old = line;

	fhandle.close(); # Close the file
	del(line, line_old)
	print(60*'-'+'\nMAX. RMS : %f'%RMSmax+' in file '+maxRMSfile);

	return(None)
# ----------------------------------------------



# ----------------------------------------------
def RenameArcs(original_path, cwd = None):
	# Change the name of the arc files in the database subforder

	if(cwd is None):
		cwd = getcwd(); # Get the current working directory

	new_path = cwd + '/database/id/'+original_path; # Directory of the new path
	new_path = new_path.replace( '//', '/');

	chdir( new_path ); # Change directory

	files = listdir( new_path );
	for name in files:
		if name.endswith('_arc.fits'):
			rename( name, name.replace('.fits','') );

	# Go back to the original w.d.
	chdir(cwd);
# --------------------------------------------



# --------------------------------------------
def Initialize(path, cwd = None, preserve_cr_files = False):
	if(cwd is None):
		cwd = getcwd(); # Get the current working directory

	# Remove all the .lis files:
	lst = listdir(cwd);

	for name in lst:
		# Remove all the .lis files
		if name.lower().split('.')[-1] == 'lis':
			remove(name);


	# Remove all the apertures:
	new_cwd = cwd + '/database';
	new_cwd = new_cwd.replace(new_cwd, '//');
	chdir( new_cwd );
	lst = listdir(cwd);

	for name in lst:
		# Remove the apertures
		if name.lower().startswith('ap_'):
			remove(name);

	chdir(cwd);
	chdir(path);

	# Remove all the .fits and .cr files:
	lst = listdir(path);
	for name in lst:
		if name.lower().endswith('.fits'):
			remove(name);
		if name.lower().endswith('ms.fit'):
			remove(name);
		if name.lower().endswith('.cr') and not(preserve_cr_files):
			remove(name);

	# Back to the original working directory
	chdir(cwd);
# --------------------------------------------------


# --------------------------------------------
def ShowResults( namelist = 'cross.lis', show = True):
	if not show: return;

	# Display spectra
	single_spectra_flag = namelist.lower().endswith('.fit') or namelist.lower().endswith('.fits');

	if single_spectra_flag:
		fhandle = [namelist];
	else:
		# Plot the spectrum of all the results:
		fhandle = open(namelist);

	plt.figure('Reduced Spectr', figsize=(19.2, 9.46) );

	for line in fhandle:
		line = line.replace('\n','');
		line = line.split('[')[0];

		fitshandle = fits.open(line);
		# For the labels
		line = line.split('/')[-1];
		line = line.replace('.fits','');
		line = line.replace('.fit','');
		line = line.replace('_out','');
		line = line.replace('_cross','');
		if(len(line)>15): line = line[-15:];

		delta_lambda = fitshandle[1].header['CDELT1'];
		lambda0 = fitshandle[1].header['CRVAL1'];
		lambda_axis = lambda0 + delta_lambda*arange(0,len(fitshandle[1].data))
		plt.plot(lambda_axis, fitshandle[1].data, label = line+': '+fitshandle[1].header['object'], lw=0.5);
		plt.xlim( [lambda_axis.min(), lambda_axis.max()]);
		fitshandle.close();
	plt.xlabel('Wavelength (A)');
	if not single_spectra_flag:
		fhandle.close();
	plt.legend()

	plt.show();
# --------------------------------------------------


# --------------------------------------------
def SaveSpectrumAsImage( namelist = 'cross.lis'):
	# Display spectra
	single_spectra_flag = namelist.lower().endswith('.fit') or namelist.lower().endswith('.fits');

	if single_spectra_flag:
		fhandle = [namelist];
	else:
		# Plot the spectrum of all the results:
		fhandle = open(namelist);
	
	if not isdir('figures'):
		mkdir('figures');
		print('figures/ created!');
		

	for line in fhandle:
		line = line.replace('\n','');
		line = line.split('[')[0];

		fitshandle = fits.open(line);

		# For the labels
		line = line.split('/')[-1];
		line = line.replace('.fits','');
		line = line.replace('.fit','');
		line = line.replace('_out','');
		line = line.replace('_cross','');
		if(len(line)>15): line = line[-15:];


		delta_lambda = fitshandle[1].header['CDELT1'];
		lambda0 = fitshandle[1].header['CRVAL1'];
		lambda_axis = lambda0 + delta_lambda*arange(0,len(fitshandle[1].data))

		# Figure:
		fig = plt.figure(figsize=(19.2, 9.46) );
		plt.plot(lambda_axis, 0*lambda_axis,'k--');
		plt.plot(lambda_axis, fitshandle[1].data, lw=0.5);
		plt.title(line+': '+fitshandle[1].header['object'])
		plt.xlim( [lambda_axis.min(), lambda_axis.max()]);
		fitshandle.close();
		plt.xlabel('Wavelength (A)');
		fig.savefig('./figures/'+line+'.png');
		plt.show();
		plt.close();
	if not single_spectra_flag:
		fhandle.close();

	return(None);
# --------------------------------------------------


# --------------------------------------------------
def CleanCosmicRays(namelist = 'cross.lis'):
	# The .cr files are saved in the same directory as the .fits files
	# Display spectra
	single_spectra_flag = namelist.lower().endswith('.fit') or namelist.lower().endswith('.fits');

	if single_spectra_flag:
		fhandle = [namelist];
	else:
		# Plot the spectrum of all the results:
		fhandle = open( namelist );

	for line in fhandle:
		line = line.replace('\n','');
		line = line.split('[')[0];

		fitshandle = fits.open( line ); # Open the .fits file
		data1D = fitshandle[1].data.copy(); # the 1D spectrum is in the extension 1
		objectname = fitshandle[1].header['object'];
		Nrows = len( data1D );
		lambda_axis = arange(0, Nrows);

		# Smooth the background (median filter):
		data1Dsmooth = medfilt( data1D, 5);

		plt.figure('Relative images ', figsize=(19.2, 9.46) );
		plt.plot( lambda_axis, data1D, 'b', linewidth=0.5);
		plt.plot( lambda_axis, data1Dsmooth,'r', linewidth=0.5);
		plt.plot( lambda_axis, 0*lambda_axis, 'k--', linewidth=0.5);
		plt.title('Object: %s. Click on the H_beta line to continue '%objectname);
		plt.xlim([lambda_axis.min(), lambda_axis.max()])
		coord = plt.ginput( timeout=120, show_clicks=True );

		if(len(coord)!=0):
			coord = array(coord)[0,0]; # Keep the first term
			aux = (lambda_axis-coord)**2
			left = (lambda_axis[aux<100])[0]
			coord = left + data1Dsmooth[aux<100].argmin(); # Find the closest minimum to the click point
			del(aux, left)

			lambda_H = (coord+1-fitshandle[1].header['CRPIX1'])*fitshandle[1].header['CD1_1']+fitshandle[1].header['CRVAL1']; # Get the wavelength of the observed reference line
			gamma = lambda_H/4862.68;
			beta = (gamma**2-1)/(gamma**2+1);
		else:
			gamma = 1.;
			beta = 0.;
		# Compute the position of the common lines:
		common_lines_pix = fitshandle[1].header['CRPIX1']-1+(gamma*Common_lines_wavelengths - fitshandle[1].header['CRVAL1'])/fitshandle[1].header['CD1_1'];
		common_lines_pix2 = fitshandle[1].header['CRPIX1']-1+(gamma*Common_lines_wavelengths2 - fitshandle[1].header['CRVAL1'])/fitshandle[1].header['CD1_1'];

		# Show the velocity
		print('Object: %s. Beta = %f. v= %f km/s '%(objectname, beta, beta*3E5) );
		plt.close();

		# Create cosmic ray file :
		# Remove the extension
		line = line.replace('.ms.fits','');
		line = line.replace('.ms.fit','');
		line = line.replace('.fits','');
		line = line.replace('.fit','');
		fw = open(line+'.cr','a'); # Append pre-existing file
		coord = array([[0,0],[0,0]]);
		while(len(coord)==2):
			plt.figure('Smoothed spectrum '+line.split('/')[-1], figsize=(19.2, 9.46) );
			data1Dsmooth = medfilt( data1D, 3);

			# Show the stacked spectrum
			plt.subplot(2,1,1)
			plt.plot( lambda_axis, data1D, linewidth = .5 );
			plt.plot( lambda_axis, data1D, 'r.');
			plt.plot( lambda_axis, data1Dsmooth, 'k', linewidth = .5);
			plt.plot( lambda_axis, 0*lambda_axis, 'k--', linewidth = .5);
			#plt.plot( common_lines_pix, 0*common_lines_pix+1.1*data1D.max(),'|'); # Overplot the lines
			plt.vlines( common_lines_pix, 0, 1.1*data1D.max(), color = 'gray', linestyles='--'); # Overplot the lines
			#plt.plot( common_lines_pix2, 0*common_lines_pix2+1.1*data1D.max(),'g|'); # Overplot the lines
			plt.vlines( common_lines_pix2, 0, 1.1*data1D.max(), color = 'lightgray', linestyles='--'); # Overplot the lines
			plt.title( "Object: %s\nFrom L to R, delete above. From Right to Left, delete below."%objectname)
			plt.xlim([lambda_axis.min(), lambda_axis.max()])

			plt.subplot(2,1,2);
			plt.plot( lambda_axis, data1D-data1Dsmooth, 'g');
			plt.xlim([lambda_axis.min(), lambda_axis.max()])
			plt.show()
			coord = plt.ginput(-1, timeout=120, show_clicks=True);
			coord = array( coord );
			plt.close(); # Close the image
			if(len(coord)!=2):
				# No points to edit
				fitshandle.close(); # Close the .fits file
			else:
				# Find all the points above/below the line AND between the inputs
				x = coord[:,0];
				y = coord[:,1];
	
				# slope=0 (always horizontal), intercept
				intercept = y.mean();
	
				# Select the points to be deleted
				is_selected = ( x.min()<lambda_axis )*( lambda_axis<x.max() );
				if(x[1]>x[0]):
					is_selected = is_selected*( data1D>intercept );
				else:
					is_selected = is_selected*( data1D<intercept );					

				# Get the index of the selected pixels
				lambda_selected = lambda_axis[is_selected];
				for n, index in enumerate(lambda_selected):
					peak_value = data1D[index];
					# The cosmic ray lies in the [index] position
					# Coordinates and weights of the neighbours:
					# W
					w1 = .5*(index!=0);
					i1 = (index-1)*(index!=0);
					# E
					w2 = .5*(index!=(Nrows-1));
					i2 = (index+1)*(index!=(Nrows-1));

					# Interpolation
					w_sum = w1 + w2;
					w1 = w1/w_sum;
					w2 = w2/w_sum;
	
					new_value = w1*data1D[i1] + w2*data1D[i2];

					# Wait! if the interpolated value is above (below) intercept, use intercept:
					if(x[1]>x[0]):
						new_value = min([new_value, intercept]);
					else:
						new_value = max([new_value, intercept]);

					# Write the output in the .cr (cosmic ray) file
					fw.write("%i %f\n"%(index+1, new_value));

					# Edit the pixel:
					data1D[index] = new_value;
		fw.close(); # Close the cosmic ray file

	if not single_spectra_flag:
		fhandle.close();

	return(False);
# ------------------------------------------------------



# ------------------------------------------------------
def AcceptButton(mssg='Accept changes?', show = True):
	# When show=False, it simply returns True
	if not show: return(True);

	plt.figure('Changes');
	plt.fill_between([0,1],[1,1],[0,0], color='green');
	plt.fill_between([1,2],[1,1],[0,0], color='red');
	plt.text(0.5,0.5,'YES',color='w');
	plt.text(1.5,0.5,'NO',color='k');
	plt.title(mssg);
	plt.axis([0,2,0,1]);
	plt.gca().set_yticks([]);
	plt.gca().set_xticks([]);
	try:
		response = plt.ginput(1, show_clicks=False);
	except:
		response = [];
	if(len(response)==0):
		response = True;
	else:
		response = response[0][0];
		response = (response<1);

	plt.close()
	return(response);
# ------------------------------------------------------



# ------------------------------------------------------
def ReadAperture(fitsname, cwd = './', lowhigh=False):
	# fitsname follows the _out.fits convention.
	# This script looks for the correspondent aperture and, if it exists, allows you to edit it
	# To do so, it reads the ap_...file in the "database" folder
	# If lowhigh=False, it also returns low and high (edges of the trace)
	

	# Remove the extension (if any):
	fitsname = fitsname.split('[')[0];

	# Check if the file exists:
	assert(isfile(fitsname)), 'ERROR in ReadApertures: No %s found'%fitsname

	apname = cwd+'/database/ap'+fitsname.replace('/','_');
	apname = apname.replace('.fits','');
	apname = apname.replace('.fit','');
	apname = apname.replace('///','/');	
	apname = apname.replace('//','/');
	apname = apname.replace('_cross','_out');
	assert(isfile(apname)), 'ERROR in ReadApertures: No %s found'%apname

	# Open the aperture file
	handle_ap = open( apname );
	txt = handle_ap.read(); # We can read the whole file because it is not very big
	handle_ap.close(); # Close the aperture file
	del(handle_ap);

	header, curvedata = txt.split('\n\tcurve');
	header = header + '\n\tcurve'; # Recover this part of the text
	curvedata = curvedata.split()
	center = header.split('aperture')[1];
	center = center.strip()
	center = float(center.split()[2]); # Now we get the center of this trace

	if(lowhigh):
		header = header.split('background')[0];
		header = header.split('\n');
		for line in header:
			line = line.strip();
			if line.startswith('low'):
				low = float(line.split()[1]);
			if line.startswith('high'):
				high = float(line.split()[1]);
		del(line)
	
	Ncoeffs = int(curvedata.pop(0));
	type_of_fit = int(float(curvedata.pop(0)));
	order = int(float(curvedata.pop(0)));
	min_line = float(curvedata.pop(0));
	max_line = float(curvedata.pop(0));
	coeffs = [float(value) for value in curvedata];
	del(value);

	# IRAF starts at 1:
	center = center -1;
	min_line +=-1;
	max_line +=-1;

	if(lowhigh):
		return(center, Ncoeffs, type_of_fit, order, min_line, max_line, coeffs, low, high)
	else:
		return(center, Ncoeffs, type_of_fit, order, min_line, max_line, coeffs)
# ------------------------------------------------------	



# ------------------------------------------------------
def EditAperture(fitsname, cwd = './', auto = True):
	# fitsname follows the _out.fits convention.
	# This script looks for the correspondent aperture and, if it exists, allows you to edit it
	# To do so, it reads the ap_...file in the "database" folder
	# If auto==True, the aperture will be edited if it is offcentered (not between L/3 and 2L/3, where L is the number of columns of the CCD)
	

	# Remove the extension (if any):
	fitsname = fitsname.split('[')[0];

	# Check if the file exists:
	assert(isfile(fitsname)), 'ERROR in EditApertures: No %s found'%fitsname

	center, Ncoeffs, type_of_fit, order, min_line, max_line, coeffs = ReadAperture(fitsname, cwd = cwd);

	# Get the size of the CCD:
	fitshandle = fits.open( fitsname );
	Next = len(fitshandle);
	for n in xrange(Next):
		# Keep the latest extension
		data2D = fitshandle[Next-n-1].data.copy();
		if( len(data2D)!=0 ):
			break;
	fitshandle.close();

	x = arange(data2D.shape[0]);
	is_selected = (min_line<=x)*(x<max_line);
	x = x[is_selected];

	# Edit the trace only when it is needed
	Nrows = data2D.shape[0];	
	Ncols = data2D.shape[1];
	if( auto and (int(Ncols/3)<center) and (center<int(2*Ncols/3) )):
		x,y = _IRAFfittingfunct( x, type_of_fit, order, min_line, max_line, coeffs);
		bkg_coords = _StackColums( data2D, x, y+center, auto=True);
		return(False)
	elif( auto and not((int(Ncols/3)<center) and (center<int(2*Ncols/3) ) ) ):
		center = _AutoTraceFinder( data2D );
		center = min([center, int(5*Ncols/6)]);
		center = max([center, int(Ncols/6)]);
		x,y = _IRAFfittingfunct( x, type_of_fit, order, min_line, max_line, coeffs);
		#for n in range(Ncols/3): data2D[:,n] = data2D[:, Ncols/3];
		#for n in range(2*Ncols/3, Ncols): data2D[:,n] = data2D[:, 2*Ncols/3]; # Fake 2d spectrm.		
		bkg_coords = _StackColums( data2D, x, y+center, auto=True);	
		#Use the output to edit the aperture:
		WriteAperture(fitsname, center, min_line, max_line, coeffs, cwd = cwd, bkg_coords = bkg_coords);
		print("Aperture edited for "+fitsname);
		return(False)

	# Now compute the fitting
	x,y = _IRAFfittingfunct( x, type_of_fit, order, min_line, max_line, coeffs);

	# Plot the spectrum and the trace
	plt.figure('Trace', figsize=(19.2, 9.46) );
	plt.suptitle('Trace of {}. Click to zoom. Enter to finish. Step 1/3'.format(fitsname));
	# Use three subplots
	S1 = plt.subplot(4,1,1);
	plt.imshow( data2D[:int(Nrows/4),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet' );
	plt.plot(x,y+center,'w--');
	plt.xlim([0,int(Nrows/4)]);
	# ----------------
	S2 = plt.subplot(4,1,2, sharey=S1);
	plt.imshow( data2D[int(Nrows/4):int(Nrows/2),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(Nrows/4), int(Nrows/2), Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(Nrows/4), int(Nrows/2)]);
	# ----------------
	S3 = plt.subplot(4,1,3, sharey=S1);
	plt.imshow( data2D[int(Nrows/2):int(3*Nrows/4),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(Nrows/2), int(3*Nrows/4), Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(Nrows/2), int(3*Nrows/4)]);
	# ----------------
	S4 = plt.subplot(4,1,4, sharey=S1);
	plt.imshow( data2D[int(3*Nrows/4):,:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(3*Nrows/4), Nrows, Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(3*Nrows/4), Nrows]);

	trash = plt.ginput(timeout=150, show_clicks=True);
	if(len(trash)==0):
		plt.close();
		# You have pressed enter. You do not want to edit this aperture
		return(False);
	else:
		trash = array(trash);
		new_center = trash[0,1];
		S1.set_ylim([new_center - int(Ncols/15), new_center + int(Ncols/15)]);
		S2.set_ylim([new_center - int(Ncols/15), new_center + int(Ncols/15)]);
		S3.set_ylim([new_center - int(Ncols/15), new_center + int(Ncols/15)]);
		S4.set_ylim([new_center - int(Ncols/15), new_center + int(Ncols/15)]);
		trash = plt.ginput(-1, timeout=120, show_clicks=True);
		if(len(trash)==0 or len(trash)==1):
			plt.close();
			# You have pressed enter. You do not want to edit this aperture
			return(False);
	trash = array(trash);
	x_new = trash[:,0];
	y_new = trash[:,1];

	# Sort the points
	y_new = y_new[x_new.argsort()];
	x_new.sort();

	# Instead of using these points, get the core of the trace for each line (row)
	# [x_new, y_new] = _MyCoreFinder(x_new, y_new, data2D); # This is an improvement
	# (deprecated. Very problematic)

	# Use linear interpolation
	[center, min_line, max_line], coeffs = _MyIRAFLinearInterpfitting( x_new, y_new);
	plt.close();


	# Display the new trace:
	x = arange(data2D.shape[0]);
	is_selected = (min_line<=x)*(x<max_line);
	x = x[is_selected];
	x,y = _IRAFfittingfunct( x, 4, len(coeffs), min_line, max_line, coeffs);
	# Plot the spectrum and the trace
	plt.figure('New Trace', figsize=(19.2, 9.46) );
	plt.suptitle('New Trace for {}. Click or Enter to finish and save. Step 2/3'.format(fitsname));
	# Use three subplots
	S1 = plt.subplot(4,1,1);
	plt.imshow( data2D[:int(Nrows/4),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet' );
	plt.plot(x,y+center,'w--');
	plt.xlim([0,int(Nrows/4)]);
	plt.ylim([center - int(Ncols/10), center + int(Ncols/10)]);
	# ----------------
	S2 = plt.subplot(4,1,2, sharey=S1);
	plt.imshow( data2D[int(Nrows/4):int(Nrows/2),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(Nrows/4), int(Nrows/2), Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(Nrows/4), int(Nrows/2)]);
	plt.ylim([center - int(Ncols/10), center + int(Ncols/10)]);
	# ----------------
	S3 = plt.subplot(4,1,3, sharey=S1);
	plt.imshow( data2D[int(Nrows/2):int(3*Nrows/4),:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(Nrows/2), int(3*Nrows/4), Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(Nrows/2), int(3*Nrows/4)]);
	plt.ylim([center - int(Ncols/10), center + int(Ncols/10)]);
	# ----------------
	S4 = plt.subplot(4,1,4, sharey=S1);
	plt.imshow( data2D[int(3*Nrows/4):,:].transpose(), vmin = percentile(data2D, 35), vmax = percentile(data2D, 85), cmap='jet', extent = [int(3*Nrows/4), Nrows, Ncols,0] );
	plt.plot(x,y+center,'w--');
	plt.xlim([int(3*Nrows/4), Nrows]);
	plt.ylim([center - int(Ncols/10), center + int(Ncols/10)]);

	trash = plt.ginput(timeout=120, show_clicks=True);
	plt.close();

	# Stack the lines of the trace
	bkg_coords = _StackColums( data2D, x, y+center, auto=False); # bkg_coords = [b_left, b_left, trace_left, trace_right, bkg_right, bkg_right] or None
	#Use the output to edit the aperture:
	WriteAperture(fitsname, center, min_line, max_line, coeffs, cwd = cwd, bkg_coords = bkg_coords);
	
	return(True)
# -------------------------------------------------------------





# -------------------------------------------------------------	
def WriteAperture(fitsname, center, min_line, max_line, coeffs, cwd = './', bkg_coords=None):
	# fitsname follows the _out.fits convention.
	# This script looks for the correspondent aperture and overwrites it
	# To do so, it reads the ap_...file in the "database" folder
	# bkg_coords = [b_left, b_left, trace_left, trace_right, bkg_right, bkg_right] or None
	
	# IRAF starts at 1
	center = center +1;

	# Remove the extension (if any):
	fitsname = fitsname.split(']')[0];

	# Check if the file exists:
	assert(isfile(fitsname)), 'ERROR in WriteApertures: No %s found'%fitsname

	apname = cwd+'/database/ap'+fitsname.replace('/','_');
	apname = apname.replace('.fits','');
	apname = apname.replace('.fit','');
	assert(isfile(apname)), 'ERROR in WriteApertures: No %s found'%apname

	# If True, python is reading the background parameters
	bkg_mode = False;

	# Open the aperture file
	handle_ap = open( apname );
	# Create the temp file:
	handle_w = open('temp.txt','w');
	for line in handle_ap:
		# This is for the 2nd line
		if line.startswith('begin	aperture'):
			elements = line.split(' ');
			elements[-2] = '%.4f'%center;
			line = '';
			for element in elements:
				line = line + element+' ';
			line = line.rstrip()+'\n'
			del(element, elements)

		# This is for the center
		if line.strip().startswith('center'):
			index = line.find('center')+7; # 7 is the length of 'center\t'
			line = line[0:index]+'%.4f'%center+'\t'+line.split()[-1]+'\n'

		# This is for the low
		if line.strip().startswith('low\t') and not(bkg_coords is None):
			index = line.find('low')+4; # 4 is the length of 'low \t'
			line = line[0:index]+str(bkg_coords[2])+'\t'+line.split()[-1]+'\n'

		# This is for the high
		if line.strip().startswith('high\t') and not(bkg_coords is None):
			index = line.find('high')+5; # 5 is the length of 'high\t'
			line = line[0:index]+str(bkg_coords[3])+'\t'+line.split()[-1]+'\n'

		# This is for the curve parameters
		if line.strip().startswith('curve'):
			line = line.split('curve')[0]+'curve\t'+'%i'%(len(coeffs)+4)+'\n'; # Write the number of coefficients+4
			handle_w.write( line ); # Write the line to the new file
			break;

		# This is for the bkg
		if not(bkg_coords is None) and line.strip().startswith('background'):
			bkg_mode = True; # Now the bkg parameters will be edited
		if( bkg_mode and line.strip().startswith('xmin ') ):
			line = line.split('xmin ')[0]+'xmin %i.\n'%bkg_coords[0];
		if( bkg_mode and line.strip().startswith('xmax ') ):
			line = line.split('xmax ')[0]+'xmax %i.\n'%bkg_coords[1];
		if( bkg_mode and line.strip().startswith('sample ') ):
			line = line.split('sample ')[0]+'sample %i:%i,%i:%i\n'%(bkg_coords[0],bkg_coords[1],bkg_coords[-2],bkg_coords[-1]);
		if( bkg_mode and line.strip().startswith('grow') ):
			bkg_mode = False; # Exit the bkg mode
	
		handle_w.write( line ); # Write the line to the new file
	line = line.split('curve')[0]+'\t'; # space
	handle_ap.close(); # Not it is manual:

	# Last term
	handle_w.write(line+'4.\n') # Linear
	handle_w.write(line+str(len(coeffs))+'\n') # Order
	handle_w.write(line+'%i.\n'%min_line) # min
	handle_w.write(line+'%i.\n'%max_line) # max
	for coef in coeffs:
		handle_w.write(line+'%f\n'%coef) # coefficient

	handle_w.close() # Close the temp file

	# Re-open the aperture file (now to edit it)
	handle_w = open( apname, 'w' );

	# Re-open the temp file (now to read it)
	handle_ap = open( 'temp.txt', 'r' );
	
	# Write the aperture file
	for line in handle_ap:
		handle_w.write( line );
	handle_ap.close();
	handle_w.close();

	remove( 'temp.txt' );

	return(None)
# -------------------------------------------------------------	



# -------------------------------------------------------------	
def _IRAFfittingfunct( x, type_of_fit, order, min_line, max_line, coeffs):
	# Using the input parameters, computes y = f(x), where f(x) is one of the fitting function (legendre, chebyshev, spline,...) given by IRAF.
	# x: independent variable
	# returns x,y where y exists (x<min_line and x>max_line are excluded)

	coeffs = array(coeffs);
	is_selected = (min_line<=x)*(x<max_line);
	x = x[is_selected];
	y = 0*x; # Dependent variable

	# Normalized variable:
	if(type_of_fit == 1):
		# Chebyshev
		n = 2*(x-.5*(max_line + min_line))/(max_line - min_line);
		z = [1.+0*n,n];
		for i in range(2,order):
			z.append(2*n*z[i-1]-z[i-2]);
		for i in range(order):
			y = y + coeffs[i]*z[i];
	elif(type_of_fit == 2):
		# Legendre
		n = 2*(x-.5*(max_line + min_line))/(max_line - min_line);
		z = [1+0*n,n];
		for i in range(2,order):
			z.append( ((2*i-3)*n*z[i-1]-(i-2)*z[i-2])/(1.0*i-1)   );
		for i in range(order):
			y = y + coeffs[i]*z[i];
	elif(type_of_fit == 3):
		# Cubic Spline
		npieces = len(coeffs)-3;
		s = npieces*(x-min_line)/(max_line-min_line);
		j = int32(s);

		a = (j+1)-s;
		b = s-j;
		y = coeffs[j]*a**3 + coeffs[j+1]*(1+3*a*(1+a*b)) + coeffs[j+2]*(1+3*b*(1+a*b)) + coeffs[j+3]*b**3;

	else:
		# Linear Spline
		npieces = len(coeffs)-1;
		s = npieces*(x-min_line)/(max_line-min_line);
		j = int32(s);

		a = (j+1)-s;
		b = s-j;
		next = j+1*(j!=npieces);
		y = coeffs[j]*a+coeffs[next]*b;

	return(x,y);
# -------------------------------------------------------------	



# -------------------------------------------------------------
def _MyIRAFLegendrefitting( x, y):
	# Calculates the coefficients of the Legendre fitting
	order = len(x);
	assert(len(x)==len(y)), 'x and y have different sizes'

	# Create a matrix of zeros
	A = array(order*[order*[0.]]);
	A = A.copy();
	Z = array(order*[order*[0.]]);
	Z = Z.copy(); # Each row is a Legendre pol., each column a point: Z[i,j]= L_i (x_j)

	# Create an array of zeros
	b = 0.*x;

	min_line = min(x);
	max_line = max(x);
	
	center = y.mean();
	y = y-center;
	
	# Norm
	n = 2*(x-.5*(max_line + min_line))/(max_line - min_line);
	
	# Fill the matrix Z:
	Z[0,:] = 1.
	Z[1,:] = n.copy();
	for counter in range(2,order):
		Z[counter,:] = (2*counter-3)*n*Z[counter-1,:] - (counter-2)*Z[counter-2,:];
		Z[counter,:] = Z[counter,:]/(1.*counter-1);

	# Fill the linear system
	for i in range(order):
		for j in range(order):
			A[i,j] = sum(Z[i,:]*Z[j,:]);
		b[i] = sum(y*Z[i,:]);

	coeffs = solve(A,b);

	return([center, min_line, max_line], coeffs)
# -------------------------------------------------------------



# -------------------------------------------------------------
def _MyIRAFLinearInterpfitting( x, y):
	# Calculates the coefficients of the linear interpolation fitting
	order = len(x);
	npieces = order-1;
	assert(len(x)==len(y)), 'x and y have different sizes'

	# Sort the nodes
	y = y[x.argsort()];
	x.sort();

	min_line = x.min();
	max_line = x.max();

	# Get the offset
	center = y.mean();
	y = y-center;

	# Compute the normalized parameter	
	s = (x-min_line)/(max_line-min_line)*npieces;

	# Use an uniform grid:
	x_uni = linspace(0, npieces, npieces+1, endpoint=True);

	# Interpolate
	y = y[s.argsort()]
	s.sort()
	f = interp1d(s, y, kind='linear', bounds_error = False, fill_value='extrapolate')

	y_uni = f(x_uni) # Use the interpolation
	coeffs = array(y_uni);

	return([center, min_line, max_line], coeffs)
# -----------------------------------------------------------------



# -------------------------------------------------------------	
def _StackColums(data2D, x, y, def_width = 50, auto = False):
	# Collapse in the direction of the trace
	# If auto, calculates the coordiantes using a Gaussian fit
	x = int32(x);
	y = int32(y+0.5); # Round
	assert(x.max()<data2D.shape[0]), 'x=%.2f is too large'%x.max()
	assert(y.max()<data2D.shape[1]), 'y=%.2f is too large'%y.max()
	assert(y.min()>=0), 'y=%.2f is too short'%y.min()


	left_min = data2D.shape[1];
	right_max = 0;
	data1D = array((2*def_width+1)*[0.]);
	for n,line in enumerate(x):
		left = max([y[n]-def_width,0]);
		right = min([y[n]+def_width+1, data2D.shape[1]])

		# Distances from the center of the trace
		d_left = y[n] - left;
		d_right = right - y[n];

		data1D_add = data2D[line, left:right];
		data1D[def_width-d_left:def_width+d_right] = data1D[def_width-d_left:def_width+d_right] + data1D_add;
	if not auto:
		coord = [1];
		while(len(coord)<6 and len(coord)>0):
			plt.figure('Stacked spectrum');
			plt.plot(arange(len(data1D))-.5*len(data1D), data1D, 'k');
			plt.title('Six clicks to set the left bkg, the width of the trace and the right bkg');
			coord = plt.ginput(6, timeout=120, show_clicks=True)
			plt.close();
		# If no changes has been applied: keep the default parameters
		if len(coord)==0:
			coord = None;
		else:
			coord = array(coord)[:,0];
			coord = [round(c) for c in coord]
			coord = array(coord);
			coord.sort();
	else:
		coord = _gaussianfit(arange(len(data1D))-.5*len(data1D), data1D);
		if(coord.optimality>0.01): print('\t WARNING: this gaussian fit may not be optimal!!!! %.3f'%coord.optimality);
		param = coord.x; # Get the parameters
		param[-1] = abs(param[-1]);
		coord = [1, param[2]-3*param[3]-100, param[2]-3*param[3], param[2]+3*param[3], param[2]+3*param[3] + 100, len(data1D)-1]
		coord = [round(c) for c in coord]
		coord = array(coord);
		coord.sort();

	return(coord);
# -------------------------------------------------------------	




# -------------------------------------------------------------	
def AutoClearCosmicRays(fitsname='cross.lis', cwd = './'):
	# fitsname follows the _cross.fits convention. It can be a .lis file or a .fits
	# Detects cosmic rays and interpolates in the 1D spectrum

	single_spectra_flag = fitsname.lower().endswith('.fit') or fitsname.lower().endswith('.fits');

	if single_spectra_flag:
		fhandle = [fitsname];
	else:
		# Plot the spectrum of all the results:
		fhandle = open( fitsname );

	for line in fhandle:
		line = line.replace('\n','');
		fitsname = line; # Replace fitsname
		line = line.replace('_out.fits', '_cross.fits');

		# Remove the extension (if any):
		line = line.split('[')[0];

		# Check if the file exists:
		assert(isfile(line)), 'ERROR in AutoClearCosmicRays: No %s found'%line

	
		# Get the size of the CCD:
		fitshandle = fits.open( fitsname );
		Next = len(fitshandle);
		# Keep the 1D and 2D spectrum
		data2D = fitshandle[0].data.copy();
		data1D = fitshandle[1].data.copy()
		fitshandle.close(); # Close the file

		# Get the info of the fitting curve
		center, Ncoeffs, type_of_fit, order, min_line, max_line, coeffs, low, high = ReadAperture(line, cwd = cwd, lowhigh=True);
		x = arange(data2D.shape[0]);
		is_selected = (min_line<=x)*(x<max_line);
		
		y = _IRAFfittingfunct( x[is_selected], type_of_fit, order, min_line, max_line, coeffs)[1];
		y = y + center; # offset in y

		z = 0.*x;
		z[:int(len(z)/2)]=y[0];
		z[int(len(z)/2):]=y[-1];
		z[is_selected] = y;

		y = z.copy();
		del(z);

		# parse x,y as integers:
		x = int32(x);
		y = int32(y);
		low = int(round(low));
		high = int(round(high));

		# Try to extract the trace by "hand"
		trace = list();

		for n in range(len(x)):	
			trace.append( 	data2D[x[n], (y[n]+low):(y[n]+high)]  )
		trace = array(trace); # From list to array

		# Normalize:
		trace = trace*1./trace.sum(0);

		# Median
		trace_median = median(trace,1);
		new_trace_median = 1.*trace_median;
		for n in range(1,len(trace_median)-1):
			new_trace_median[n] = (trace_median[n-1] + trace_median[n] + trace_median[n+1])/3.;
		trace_median = new_trace_median.copy();
		del(new_trace_median);

		# Normalize again:
		Normlz = ((data1D*trace_median)[data1D>0]).sum()/((trace_median[data1D>0])**2).sum();
		trace_median = Normlz*trace_median;
		trace = trace*Normlz;

		var_trace = 0.*trace;
		for n in range(trace.shape[1]):
			var_trace[:,n] = (trace[:,n] - trace_median)**2;
		var_trace = median(var_trace,1);
		
		# Flip:
		var_trace = var_trace[-1::-1];
		trace_median = trace_median[-1::-1];
		trace = trace[-1::-1,:];

		# lsq fitting using data1D as template:
		a11 = (trace_median**2).sum();
		a12 = trace_median.sum();
		a22 = len(trace_median);
		b1 = (trace_median*data1D).sum();
		b2 = data1D.sum();

		scale_term = (b1*a22-b2*a12)/(a11*a22-a12*a12);
		offset_term = (a11*b2-a12*b1)/(a11*a22-a12*a12);
		del(a11, a12, a22, b1, b2);

		# Apply changes (update):
		trace_median = scale_term*trace_median + offset_term;
		var_trace = scale_term*var_trace*scale_term;

		# Fit each window of the median_trace to fit the data:
		pixel_axis = arange(len(data1D));
		pixels_selected = (pixel_axis!=pixel_axis);

		L_window = 200; # pixels
		n_div = 10; # tunes the shift
		end = -L_window;
		shift = int(L_window/n_div);

		fake_spectrum = n_div*[0.*pixel_axis];
		fake_spectrum = array(fake_spectrum).reshape(n_div,len(data1D));
		
		counter = 0;
		for counter in range(n_div):
			ini = counter*shift-shift;
			end = -L_window;
			while( end<len(data1D)-L_window ):
				ini = ini + shift;
				end = ini + L_window;
				end = min([end, len(data1D)]);

				# Get one portion of the data
				pixels_selected[ini:end] = True;
				data1D_window = data1D[pixels_selected];			

				scale_term, offset_term, shift_term = _find_best_fit(data1D_window, trace_median, ini);

				fake_spectrum[counter,ini:end] = scale_term*trace_median[ini+shift_term:end+shift_term] + offset_term;
				# Un-select the pixels
				pixels_selected[ini:end] = False;


		fake_spectrum = median(fake_spectrum,0);


		# Analize the "variance":
		var_trace = abs(data1D-fake_spectrum);
		threshold = 12.5*median(var_trace);
		bad_pixel_candidates = var_trace.argsort()[-1:-25:-1];
		bad_pixel_candidates = bad_pixel_candidates[var_trace[bad_pixel_candidates]>threshold];

		# Create cosmic ray file :
		# Remove the extension
		line = line.replace('.ms.fits','');
		line = line.replace('.ms.fit','');
		line = line.replace('.fits','');
		line = line.replace('.fit','');
		fw = open(line+'.cr','a'); # Append pre-existing file
		# append the new value to the cosmic ray file
		for n in bad_pixel_candidates:
			if (var_trace[n]>threshold or data1D[n]<-threshold/3.):
				# Interpolation using linear fitting
				closepixels = arange(n-250, n+250);
				closepixels = closepixels[closepixels>=0];
				closepixels = closepixels[closepixels<len(data1D)];
				fluxvalues = data1D[closepixels];
				is_selected = var_trace[closepixels]<threshold;
				closepixels = closepixels[is_selected];
				fluxvalues = fluxvalues[is_selected];
				if(len(closepixels)>1): 
					slope = (closepixels*fluxvalues).mean() - closepixels.mean()*fluxvalues.mean();
					slope = slope/( (closepixels**2).mean() - ( closepixels.mean() )**2);
					value = fluxvalues.mean() + slope*(n-closepixels.mean())
					data1D[n] = value;
					fw.write("%i %f\n"%(n+1, value));# +1 because of IRAF convention
				continue;

		# More bad pixel in the edges of the spectrum where 0 is expected:
		Continuous = _AutoContinuous(data1D)[:450]; # The first part
		difference = median(abs(Continuous-data1D[:450]));
		bad_pixel_candidates =  arange(450)[abs(data1D[:450])>5*difference];

		for n in bad_pixel_candidates:
			# Interpolation using linear fitting
			closepixels = arange(n-250, n+250);
			closepixels = closepixels[closepixels>=0];
			closepixels = closepixels[closepixels<700];
			fluxvalues = data1D[closepixels];
			is_selected = fluxvalues<5*difference;
			closepixels = closepixels[is_selected];
			fluxvalues = fluxvalues[is_selected];
			if(len(closepixels)>1): 
				slope = (closepixels*fluxvalues).mean() - closepixels.mean()*fluxvalues.mean();
				slope = slope/( (closepixels**2).mean() - ( closepixels.mean() )**2);
				value = fluxvalues.mean() + slope*(n-closepixels.mean())
				data1D[n] = value;
				fw.write("%i %f\n"%(n+1, value));# +1 because of IRAF convention
			continue;		

		fw.close(); # Close the cosmic ray file
		del(is_selected);

	if not single_spectra_flag:
		fhandle.close();

	return(False);
# -------------------------------------------------------------





# -------------------------------------------------------------	
def _find_best_fit(data1D_window, trace_median, ini):
	# Shifts and shrinks each portion of the trace_median to fit the portion of the data (data1D_window)
	N = len(data1D_window);
	shift_arr = arange(ini-15, ini+15);
	shift_arr = shift_arr[shift_arr>=0];
	shift_arr = shift_arr[shift_arr<(len(trace_median)-N+1)];
	
	error = list();
	scale_term = list();
	offset_term = list();

	b2 = data1D_window.sum(); # This does no change between iteractions

	for shift in shift_arr:
		trace_tmp = trace_median[shift:shift+N];

		# lsq fitting using data1D as template:
		a11 = (trace_tmp**2).sum();
		a12 = trace_tmp.sum();
		b1 = (data1D_window*trace_tmp).sum();

		scale_term.append( (b1*N-b2*a12)/(a11*N-a12*a12) );
		offset_term.append( (a11*b2-a12*b1)/(a11*N-a12*a12) );

		error.append( (data1D_window-scale_term[-1]*trace_tmp-offset_term[-1])**2 );
		error[-1] = error[-1].mean();
	
	error = array(error).argmin();
	# Extract the minimum
	scale_term = scale_term[error];
	offset_term = offset_term[error];
	shift_term = shift_arr[error];

	return(scale_term, offset_term, shift_term-ini)
# -------------------------------------------------------------		


# -------------------------------------------------------------
def _gaussianfit(x, y):
	seed = array([0,0,0.,0.]);
	seed[0] = median([ median(y[:int(len(y)/3)]), median(y[int(len(y)/3):int(2*len(y)/3)]), median(y[int(2*len(y)/3):]) ] );
	seed[1] = y.max()-seed[0];
	seed[2] = x[y.argmax()];
	seed[3] = (y.sum()-seed[0])/2.5/seed[1];
	
	def gaussian(param, x,y):
		return	param[0] + param[1]*exp(-0.5*((x-param[2])/param[3])**2)-y;
	res_lsq = least_squares(gaussian, seed, args=(x, y), verbose=False);
	return(res_lsq);
# -------------------------------------------------------------


# -------------------------------------------------------------
def _AutoTraceFinder(data2D):
	Nrows, Ncols = data2D.shape;
	data_extract = data2D[int(Nrows/5):int(4*Nrows/5), int(Ncols/3):int(2*Ncols/3)];
	data_extract = data_extract.copy();

	data_extract = medfilt( data_extract, (5,3));
	max_pos = data_extract.argmax(0);

	return( int(Ncols/3)+median(max_pos) );
# -------------------------------------------------------------

# -------------------------------------------------------------
def _AutoContinuous(data1D):
	# Get the envelope of the spectrum
	L_window = 101;
	Cont = medfilt(data1D, L_window);

	return( Cont );
# -------------------------------------------------------------

# -------------------------------------------------------------	
#def _MyCoreFinder(x_new, y_new, data2D):
#	# Get the mode/peak of each line of the trace
#
#	lines_of_the_trace = arange(int32(x_new.min()), int32(x_new.max()+1),1);
#
#	# Interp. function:
#	y_new = y_new[x_new.argsort()];
#	x_new.sort();
#	f = interp1d(x_new, y_new, kind='linear', bounds_error = False, fill_value='extrapolate');
#
#	# Get the number of columns
#	Ncols = data2D.shape[1];
#
#	y_central = f(lines_of_the_trace);
#	y_central = int32(y_central);
#
#	# Loop over the lines of the trace
#	for n,line in enumerate(lines_of_the_trace):
#		left = max([y_central[n]-25, 0]);
#		right = min([y_central[n]+25, Ncols-1]);
#		data1D = data2D[line, left:right+1].copy();
#		# Median filter:
#		data1D = medfilt( data1D, 5); # Use an smoothed version (somehow aggresive)
#		data1D[data1D<0] = 0.;
#		
#		# Keep the 90/100 of the total flux
#		total_flux = data1D.sum();
#		l_offset = 1;
#		r_offset = 1;
#
#		# Edge
#		l_edge = max([data1D.argmax() - l_offset, 0]);
#		r_edge = min([data1D.argmax() + r_offset, len(data1D)-1]);
#
#		F = data1D[l_edge:r_edge+1].sum();
#
#		while(F<.9*total_flux and total_flux>0):
#			# ----------------
#			l_edge = max([data1D.argmax() - l_offset-1, 0]);
#			F1 = data1D[l_edge:r_edge+1].sum();
#			l_edge = max([data1D.argmax() - l_offset, 0]);
#			# ----------------
#			r_edge = min([data1D.argmax() + r_offset+1, len(data1D)-1]);
#			F2 = data1D[l_edge:r_edge+1].sum();
#			r_edge = min([data1D.argmax() + r_offset, len(data1D)-1]);
#			# ----------------
#
#			if(F1>F2):
#				l_offset +=1;
#				l_edge = max([data1D.argmax() - l_offset, 0]);
#				F = F1;
#			else:
#				r_offset +=1;
#				r_edge = min([data1D.argmax() + r_offset, len(data1D)-1]);
#				F = F2;
#
#
#		y_central[n] = left + int32( (l_edge+r_edge)/2. );
#
#		f = interp1d( lines_of_the_trace, y_central, kind='linear', bounds_error = False, fill_value='extrapolate');
#		y_central = f(x_new);
#
#	return([lines_of_the_trace, y_central]);
#			



# -------------------------------------------------------------	
def AutoClearCosmicRaysPrevious(fitsname='out.lis', cwd = './'):
	# fitsname follows the _out.fits convention. It can be a .lis file or a .fits
	# Detects cosmic rays and interpolates in the 1D spectrum
	# It uses another approach compared to AutoClearCosmicRays (Gaussian fitting in the aperture region), and must be used with the 2D spectrum
	assert(False), 'Not finished'

	single_spectra_flag = fitsname.lower().endswith('.fit') or fitsname.lower().endswith('.fits');

	if single_spectra_flag:
		fhandle = [fitsname];
	else:
		# Plot the spectrum of all the results:
		fhandle = open( fitsname );

	for line in fhandle:
		line = line.replace('\n','');
		fitsname = line; # Replace fitsname
		line = line.replace('_out.fits', '_cross.fits');

		# Remove the extension (if any):
		line = line.split('[')[0];

		# Check if the file exists:
		assert(isfile(line)), 'ERROR in AutoClearCosmicRays: No %s found'%line

	
		# Get the size of the CCD:
		fitshandle = fits.open( fitsname );
		Next = len(fitshandle);
		# Keep the 1D and 2D spectrum
		data2D = fitshandle[0].data.copy();
		data1D = fitshandle[1].data.copy()
		fitshandle.close(); # Close the file

		# Get the info of the fitting curve
		center, Ncoeffs, type_of_fit, order, min_line, max_line, coeffs, low, high = ReadAperture(line, cwd = cwd, lowhigh=True);
		x = arange(data2D.shape[0]);
		is_selected = (min_line<=x)*(x<max_line);
		
		y = _IRAFfittingfunct( x[is_selected], type_of_fit, order, min_line, max_line, coeffs)[1];
		y = y + center; # offset in y

		z = 0.*x;
		z[:int(len(z)/2)]=y[0];
		z[int(len(z)/2):]=y[-1];
		z[is_selected] = y; # Extends the edges so that the trace is constant

		y = z.copy();
		del(z);

		# parse x,y as integers:
		x = int32(x);
		y = int32(y);
		low = int(round(low));
		high = int(round(high));

		# Try to extract the trace by "hand"
		trace = list();

		for n in range(len(x)):	
			trace.append( 	data2D[x[n], (y[n]+low):(y[n]+high)]  )
		trace = array(trace, dtype=float32); # From list to array

		# Normalize:
		trace = trace*1./abs(trace.sum(0));

		# Median, min and max
		trace_median = median(trace,1);
		new_trace_median = 1.*trace_median;
		for n in range(1,len(trace_median)-1):
			new_trace_median[n] = (trace_median[n-1] + trace_median[n] + trace_median[n+1])/3.;
		trace_median = new_trace_median.copy();
		del(new_trace_median);



		# Not finished

# -------------------------------------------------------------




# ------------------------------------------------------------
def GetDopplerVelocity(filelist):
	# filelist: an array of filenames or a single filename
	if type(filelist)==str:
		filelist = [filelist];

	# Reference lines:
	reference_lines = array([3934.777, 3969.588, 3971.19, 4072.3, 4102.89, 4305.61, 4341.68, 4862.68]);# ['CaII K','CaII H','He','SII','Hd','G','Hg', 'Hb'];
	velocity_of_the_object = [];
	# Loop over all the files
	for filename in filelist:
		fhandle = fits.open( filename );
		y = fhandle[len(fhandle)-1].data; # Get the last image
		lambda0 = float(fhandle[len(fhandle)-1].header['CRVAL1']);
		pix1 = int(fhandle[len(fhandle)-1].header['CRPIX1']);
		delta_lambda = float(fhandle[len(fhandle)-1].header['CDELT1']);
		
		# Get the wavelength axis:
		wl = lambda0 + delta_lambda*(arange(len(y))-pix1);

		# Compute the binned y array
		L = 30;
		ini = 0;
		end = L;
		y_median = 0.*y;
		y_std = 0.*y;
		while(end<len(y)):
			y_median[ini:end] = median(y[ini:end]);
			std_tmp = median(fabs(y[ini:end]-y_median[ini:end]));
			y_std[ini:end] = std_tmp;
			ini = end;
			end = end+L;
		del(std_tmp, ini, end);

		# Select all the local minimum:
		is_selected = (y>y);# An array of zeros
		for n in range(1,len(y)-1):
			is_selected[n] = (y[n-1]>y[n])*(y[n]<=y[n+1]);

		# These local minima must be 2 sigma below y_median:
		is_selected = is_selected*(y<(y_median-2*y_std) );
		del(y_std);

		# And must the the minima among the minima:
		#order = arange(len(y))[is_selected];
		#for n in range(1,len(order)-1):
		#	is_selected[order[n]] = (y[order[n-1]]>y[order[n]])*(y[order[n]]<=y[order[n+1]]);

		# Hdelta must be in a bin that is larger than the median value of medians (it starts close to the maximum):
		is_candidate_Hb = is_selected*(y_median>median(y_median));

		# Candidates to held H_beta:
		x_cand = wl[is_candidate_Hb];
		y_cand = y[is_candidate_Hb];
		del(is_candidate_Hb);

		# Candidates to held other lines:
		x_abs = wl[is_selected];
		y_abs = (y_median-y)[is_selected]; # Always positive by exclusion

		del(y_median);
		assert(y_abs.min()>0), 'y_abs must be positive'

		vel_array = 0.*x_cand;
		std_vel_array = 0.*x_cand;
		weight_array = 0.*x_cand;
		for n_cand, cand in enumerate(x_cand):
			# If this point were H_beta, get the positions of the rest of the reference lines
			reference_shifted = reference_lines*cand/4862.68;
			# Find the closest observed min to each reference_shifted
			weight = 0.*reference_shifted;
			vel_inferred = 0.*weight;

			# Loop over all the lines to validate
			for n_ref_shftd, ref_shftd in enumerate(reference_shifted):
				shift = fabs(x_abs-ref_shftd); # Difference between the observed position and the expected one
				index_observed_reference_shifted = shift.argmin();
				x_observed_reference_shifted = x_abs[index_observed_reference_shifted];
				y_observed_reference_shifted = y_abs[index_observed_reference_shifted];
				# This implies the following velocity
				z = (x_observed_reference_shifted-reference_lines[n_ref_shftd])/reference_lines[n_ref_shftd];
				#vel_inferred[n_ref_shftd] = 3.E5*((1.+z)**2-1.)/((1.+z)**2+1.); # In km/s
				vel_inferred[n_ref_shftd] = 3.0E5*z;
				# Use this weight
				weight[n_ref_shftd] = y_observed_reference_shifted*exp(-.5*(vel_inferred[n_ref_shftd]/500.)**2); # The closer and the lower flux, the larger the weight is

			# sort the weights and exclude the worst case
			order = weight.argsort()[1:];
			weight = weight[order];
			vel_inferred = vel_inferred[order];
			#weight = weight/vel_inferred.std();
			vel_array[n_cand] = median(vel_inferred);
			std_vel_array[n_cand] = vel_inferred.std();
			weight_array[n_cand] = y[n_cand]+weight.sum();
			del(vel_inferred, z, weight);
		# End of the loop over the H_beta candidates
		vel_array = vel_array[1:-1];
		std_vel_array = std_vel_array[1:-1];
		weight_array = weight_array[1:-1];

		# Save the velocity with maximum weight
		n_cand = weight_array.argmax(); # Best candidate (max. weight)
		velocity_of_the_object.append(vel_array[n_cand]);
		print('Velocity obtained for '+filename+' : %.2f+/-%.2f'%(vel_array[n_cand], std_vel_array[n_cand] ) );
		del(vel_array);
	velocity_of_the_object = float32(velocity_of_the_object);
	fig = plt.figure(filename.replace('.fits',''), figsize=(19.2, 9.46) );
	cand = x_cand[n_cand+1];
	reference_shifted = reference_lines*cand/4862.68;
	plt.plot(x_cand[n_cand+1],y_cand[n_cand+1],'r+');
	plt.vlines(reference_shifted, 0,y.max(),color='k',linestyle='--');
	plt.vlines(Common_lines_wavelengths*cand/4862.68, 0,y.max(),color='grey',linestyle='--');
	plt.plot(wl, y);
	fig.savefig((filename.split('/')[-1]).replace('.fits','.png') );
	plt.close('all');
	return(velocity_of_the_object);
#---------------------------------------------



# --------------------------------------------
def ReadGridHeader(filename):
	# Read the grid used by FERRE
	fhandle = open(filename);
	# Read the first line:
	first_line = fhandle.readline().upper().strip()[0];
	number_header_lines = 1;
	assert( first_line.startswith('&')),filename+' not a model file.'

	FERRE_dict = dict();

	for line in fhandle:
		line = line.lower().strip();
		keyword = line.split()[0];
		if( keyword=='/'):
			number_header_lines+=1;
			break;
		if(keyword.startswith('label')):
			keyword = keyword.split(')')[-1];
			FERRE_dict['label'] = FERRE_dict.get('label',[])+[keyword];
			continue;
		if( keyword not in _FERRE_keywords):
			break;
		else:
			FERRE_dict[keyword] = line[len(keyword):];
			number_header_lines+=1;
	fhandle.close();

	# Mandatory items:
	FERRE_dict['n_of_dim'] = int(FERRE_dict['n_of_dim']);
	FERRE_dict['npix'] = int(FERRE_dict['npix']);
	FERRE_dict['n_p'] = int32(FERRE_dict['n_p']);
	FERRE_dict['llimits'] = int32(FERRE_dict['llimits']);
	FERRE_dict['steps'] = int32(FERRE_dict['steps']);

	# Now try to understand eack keyword:
	for key,value in FERRE_dict.iteritems():
		if(key=='multi'): FERRE_dict[key] = int(value);
		if(key=='npca'): FERRE_dict[key] = int32(value);
		if(key=='transposed'): FERRE_dict[key] = int(value);
		if(key=='wave'): FERRE_dict[key] = float32(value);
		if(key=='logw'): FERRE_dict[key] = int(value);
		if(key=='vacuum'): FERRE_dict[key] = int(value);
		if(key=='resolution'): FERRE_dict[key] = float(value);
		if(key=='original_sampling'): FERRE_dict[key] = float(value);
		if(key=='synspec'): FERRE_dict[key] = int(value);
		if(key=='modo'): FERRE_dict[key] = int(value);
		if(key=='invalid_code'): FERRE_dict[key] = float(value);
		if(key=='constant'): FERRE_dict[key] = float(value);
		if(key=='continuum'): FERRE_dict[key] = float32(value);
		if(key=='precontinuum'): FERRE_dict[key] = float32(value);
	FERRE_dict['nheaderlines'] = number_header_lines;
	FERRE_dict['filename'] = filename;

	return(FERRE_dict);

# --------------------------------------------



# --------------------------------------------
def ReadGridData(FERRE_dict):
	# Uses the dictionary returned by ReadGridHeader
	pass;
