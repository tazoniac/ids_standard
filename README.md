**IDSpipeline tutorial**

This pipeline was designed to reduce the long-slit spectra obtained with the IDS spectrograph (INT, La Palma).
**IDSpipeline** is composed mainly by two files:
	1. The executable script *pipe.py*
	2. The module *INTtools.py*.

** How to use IDSpipeline **

-Put INTtools.py and pipe.py in the same directory (for example, in a folder named IDSpipeline).
-Open pipe.py and change the value of the variable *path* in order to indicate the spectral data folder of *one* night. For example, in our case:
	path = '/scratch/pedroap/data/20180615/'
 -Open ipython and type: run pipe.py
 
 IDSpipeline will read the log file created by the IDS software and recognize which ".fit" files correspond to objects, flats, bias and arcs (filters are excluded).
 It is worth mentioning that the original .fit files are not modified but copied.
 
 ** Specific options of IDSpipeline **
 
1. *Cosmic Ray correction*
1.1 use_previous_cosmicrayfiles = True/False; # If True, read the .cr files to exclude the cosmic rays detected in previous analysis of the same .fit file
2.1 AutoCosmicClean_flag = True/False; # If True, the correction is performed by the algorithm automatically
3.1 ManualCosmicClean_flag = True/False;# If True, the user can correct the spectrum by himself/herself

2. *Flat*
2.1 use_spatial_flat = False; # Keep this as False
2.2 multiflat_flag = False; # The algorithm will detect and combine all the flat files in the "sibling folders" of the one referred in the variable "path"

3. *Zero (Bias) parameters*
apply_bias_correction = False; # zero image will be calculated anyway, but it won't be applied if False

4. *Trace*:
improve_traces = True; # If False, use the trace estimated by IRAF. If True, the user will have the opportunity to edit the trace by hand
autoaperture_flag = False; # If True, INTtools will find the aperture automatically

5. *Reference arc:*
arc_ref_file = '/scratch/pedroap/data/20180616/r1393218.fit' # Either None or a .fits/.fit file. When None, the user has to identify the lines individually




 
